package com.kitlabs.androidbasicarchitecture.others

import android.content.Context
import android.os.Bundle
import android.view.Window
import androidx.annotation.RawRes
import com.kitlabs.androidbasicarchitecture.R
import com.kitlabs.androidbasicarchitecture.base.BaseDialog

class Loader(context: Context) : BaseDialog(context) {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setCancelable(false)
        requestWindowFeature(Window.FEATURE_NO_TITLE)
        setContentView(R.layout.loader_dialog)
        setDimBlur(window)
    }
}