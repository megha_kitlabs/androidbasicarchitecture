package com.kitlabs.androidbasicarchitecture.others;

import android.content.Context;
import android.content.Intent;
import com.kitlabs.androidbasicarchitecture.base.BaseActivity;

public class IntentUtils {

    public static Intent getIntent(Context context, Class<? extends BaseActivity> toClass){
        return new Intent(context, toClass.getClass());
    }

}
