package com.kitlabs.androidbasicarchitecture.others

import androidx.databinding.BindingAdapter
import com.google.android.material.textfield.TextInputLayout
import com.kitlabs.androidbasicarchitecture.base.BaseModel


/**
 * Custom Binding with Extension Functions
 * */

@BindingAdapter("setError")
fun TextInputLayout.setError(errorModel: BaseModel?) {
    errorModel?.let {
        if (!it.status)
            error = it.message
    }
}

