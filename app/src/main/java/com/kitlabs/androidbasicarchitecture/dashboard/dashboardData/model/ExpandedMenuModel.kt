package com.kitlabs.androidbasicarchitecture.dashboard.dashboardData.model

data class ExpandedMenuModel(val title : String, val icon : Int)
