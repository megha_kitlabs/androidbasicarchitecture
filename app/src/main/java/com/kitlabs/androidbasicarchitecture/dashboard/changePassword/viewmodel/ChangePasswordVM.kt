package com.kitlabs.androidbasicarchitecture.dashboard.changePassword.viewmodel

import android.util.Log
import android.widget.TextView
import androidx.databinding.BaseObservable
import androidx.databinding.ObservableField
import androidx.lifecycle.MutableLiveData
import com.airbnb.lottie.LottieAnimationView
import com.google.gson.Gson
import com.google.gson.JsonObject
import com.kitlabs.androidbasicarchitecture.base.BaseModel
import com.kitlabs.androidbasicarchitecture.dashboard.changePassword.model.ChangePasswordBean
import com.kitlabs.androidbasicarchitecture.network.RetrofitApi
import com.kitlabs.androidbasicarchitecture.network.RetrofitClient
import com.kitlabs.androidbasicarchitecture.others.Cons
import com.kitlabs.androidbasicarchitecture.others.MyUtils
import org.json.JSONObject
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class ChangePasswordVM : BaseObservable(){

    /** Initialize Text Field. */
    var currentPassword = ObservableField<String>()
    var newPassword = ObservableField<String>()
    var confirmPassword = ObservableField<String>()

    /** Initialize Validate Fields. */
    var isValidCurrentPassword = ObservableField(BaseModel(true))
    var isValidNewPassword = ObservableField(BaseModel(true))
    var isValidConfirmPassword = ObservableField(BaseModel(true))

    /** Initialize API Response. */
    var responseLiveData: MutableLiveData<ChangePasswordBean>? = null
    var errorLiveData = MutableLiveData<String>()

    init {
        responseLiveData = MutableLiveData<ChangePasswordBean>()
        errorLiveData = MutableLiveData<String>()
    }

    /**
     * On button click
     * It will check first validations.
     * If all fields valid then Go for Api Access.
     * */
    fun submitData(tvUpdate: TextView, loader: LottieAnimationView) {
        if (checkValidation()) {
            val map: HashMap<String, Any> = HashMap()
            map[Cons.currentPassword] = currentPassword.get().toString().trim()
            map[Cons.newPassword] = newPassword.get().toString().trim()
            map[Cons.confirmPassword] = confirmPassword.get().toString().trim()
            changePassword(map, tvUpdate, loader)
        }
    }

    /** Check Validation
     * If all fields valid then return true
     * */
    private fun checkValidation(): Boolean {
        var valid = false
        when {
            MyUtils.isEmptyString(currentPassword.get()) -> {
                isValidCurrentPassword.set(BaseModel(message = "Enter current password"))
            }
            MyUtils.isEmptyString(newPassword.get()) -> {
                isValidNewPassword.set(BaseModel(message = "Enter new password"))
            }
            MyUtils.isEmptyString(confirmPassword.get()) -> {
                isValidConfirmPassword.set(BaseModel(message = "Enter confirm password"))
            }
            newPassword.get() != confirmPassword.get() -> {
                isValidNewPassword.set(BaseModel(message = "New password and Confirm password should be same"))
            }
            (newPassword.get().toString().trim().length < 6) -> {
                isValidNewPassword.set(BaseModel(message = "Password should be at least minimum 6 characters"))
            }
            (newPassword.get().toString().trim().length > 16) -> {
                isValidNewPassword.set(BaseModel(message = "Password should be maximum of 16 characters"))
            }
            else -> {
                valid = true
            }
        }
        return valid
    }

    /** Change User Password*/
    private fun changePassword(hashMap: HashMap<String, Any>, tvUpdate: TextView, loader: LottieAnimationView) {
        MyUtils.viewGone(tvUpdate)
        MyUtils.viewVisible(loader)
        val passwordRequest = RetrofitClient.getUserDetails().create(RetrofitApi::class.java)
        passwordRequest.changePassword(hashMap).enqueue(object : Callback<JsonObject> {
            override fun onResponse(call: Call<JsonObject>, response: Response<JsonObject>) {
                try{
                    if (response.code() == 200 && response.isSuccessful){
                        val getResponse = Gson().fromJson(response.body(), ChangePasswordBean::class.java)
                        responseLiveData?.value = getResponse
                    }
                    else{
                        val jObjError = response.errorBody()?.string()?.let { JSONObject(it) }
                        errorLiveData.value = jObjError?.getString("message")
                    }
                } catch (e : Exception){
                    Log.e("OnException", e.localizedMessage)
                }
                MyUtils.viewGone(loader)
                MyUtils.viewVisible(tvUpdate)
            }

            override fun onFailure(call: Call<JsonObject>, t: Throwable) {
                Log.e("OnFailure", t.localizedMessage)
                MyUtils.viewGone(loader)
                MyUtils.viewVisible(tvUpdate)
            }
        })
    }

}