package com.kitlabs.androidbasicarchitecture.dashboard.updateProfile.ui

import android.content.Intent
import android.content.IntentFilter
import android.net.ConnectivityManager
import android.net.Uri
import android.os.Bundle
import android.provider.MediaStore
import android.widget.ImageView
import androidx.appcompat.app.AppCompatActivity
import androidx.core.content.FileProvider
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import com.bumptech.glide.Glide
import com.bumptech.glide.load.engine.DiskCacheStrategy
import com.kitlabs.androidbasicarchitecture.R
import com.kitlabs.androidbasicarchitecture.base.PermissionActivity
import com.kitlabs.androidbasicarchitecture.dashboard.updateProfile.viewmodel.ProfileVM
import com.kitlabs.androidbasicarchitecture.databinding.ActivityUpdateProfileBinding
import com.kitlabs.androidbasicarchitecture.others.*
import java.io.File
import java.io.IOException


class UpdateProfileActivity : PermissionActivity() {

    lateinit var binding : ActivityUpdateProfileBinding
    var viewModel: ProfileVM = ProfileVM()
    private var fil: File? = null
    private val SELECT_PICTURE = 27
    private val REQUEST_IMAGE_CAPTURE = 250
    private var imageSize = 0.0
    private var currentPhotoPath = ""
    private val imageHelper = ImageHelper()
    private val neTWorkChange = NeTWorkChange()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView(this, R.layout.activity_update_profile)
        binding.viewModel = viewModel
        validateInputType()
        viewModel.getUserDetails(this, binding.profileImage)

        //method for clickable views
        setClicks()

        bindObserver()
    }

    /**
     * observe API Status
     * show toast according to response
     */
    private fun bindObserver() {
        viewModel.updateLiveData?.observe(this, Observer {
            if (it != null) {
                if (it.status == "success") {
                    Toaster.shortToast(it.message)
                    onBackPressed()
                } else {
                    Toaster.shortToast(it.message)
                }
            }
        })

        viewModel.errorLiveData.observe(this, Observer {
            Toaster.shortToast(it)
        })
    }

    /** for handling clicks */
    private fun setClicks() {
        /** Button @Back click listener */
        binding.buttonBack.setOnClickListener {
            onBackPressed()
        }

        /** @Update User Profile click listener */
        binding.buttonSave.setOnClickListener {
            viewModel.updateData(fil, binding.tvSave, binding.loader)
        }

        /** Button for choosing profile picture from camera or gallery click listener
         * it will check first permissions.
         * if permissions allow than show dialog
         * */
        binding.chooseImage.setOnClickListener {
            if (hasCameraAndStoragePermission()) {
                showPictureAndCameraPopup()
            }
        }
    }

    /** Dialog has camera and gallery options.
     * Perform functionality according to user selection
     * */
    private fun showPictureAndCameraPopup() {
        showPicturePickerDialog(object : CallBack<Int>() {
            override fun onSuccess(t: Int?) {
                when (t) {
                    1 -> {
                        picturePickerDialog?.hide()
                        takePhotoFromCamera()
                    }
                    2 -> {
                        picturePickerDialog?.hide()
                        takePhotoFromGallery()
                    }
                }
            }
        })
    }

    /** for choosing image from gallery */
    private fun takePhotoFromGallery() {
        val galleryIntent = Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI)
        startActivityForResult(galleryIntent, SELECT_PICTURE)
    }

    /** for clicking image from camera */
    private fun takePhotoFromCamera() {
        val takePictureIntent = Intent(MediaStore.ACTION_IMAGE_CAPTURE)
        if (takePictureIntent.resolveActivity(this.packageManager) != null) {
            var photoFile: File? = null
            try {
                photoFile = imageHelper.createImageFile(this)
                currentPhotoPath = photoFile.absolutePath
            } catch (ex: IOException) {
                ex.printStackTrace()
            }
            if (photoFile != null) {
                val photoURI = FileProvider.getUriForFile(this, "com.androidbasicarchitecture.android.fileprovider", photoFile)
                takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT, photoURI)
                startActivityForResult(takePictureIntent, REQUEST_IMAGE_CAPTURE)
            }
        }
    }

    /**
     * @Gallery
     * If image from gallery than it compress the image to target MB
     * Pass the image and path to setImage method
     * @Camera
     * Else if the image is from camera than check the rotation
     * Compress the image to target MB
     * Pass the image and path to setImage method
     * */
    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)

        if (requestCode == REQUEST_IMAGE_CAPTURE && resultCode == AppCompatActivity.RESULT_OK) {
            val f = File(currentPhotoPath)
            imageSize = imageHelper.compressImage(f.absolutePath, 2.0)
            setImage(binding.profileImage,  f.absolutePath)
        }
        else if (requestCode == SELECT_PICTURE && resultCode == AppCompatActivity.RESULT_OK) {
            val selectedImageUri: Uri? = data?.data
            if (selectedImageUri != null) {
                imageHelper.uriToFile(this, selectedImageUri).let { file ->
                    imageSize = imageHelper.compressImage(file!!.absolutePath, 2.0)
                    setImage(binding.profileImage, file.absolutePath)
                }
            }
        }
    }

    /**
     * Save image in file
     * Set the image in image view
     * */
    private fun setImage(imageView: ImageView, filePath: String){
        fil = File(filePath)
        Glide.with(imageView.context).asBitmap().load(filePath).skipMemoryCache(true)
            .diskCacheStrategy(DiskCacheStrategy.NONE).into(imageView)
    }

    /** TextWatcher that watch a input text field and can instantly update data */
    private fun validateInputType() {
        binding.etFirstName.addTextChangedListener(
            CustomWatcher(binding.etFirstName,
            binding.tilFirstName, "Enter first name", CustomWatcher.EditTextType.FirstName)
        )
        binding.etLastName.addTextChangedListener(
            CustomWatcher(binding.etLastName,
                binding.tilLastName, "Enter last name", CustomWatcher.EditTextType.LastName)
        )
        binding.etLocationProfile.addTextChangedListener(
            CustomWatcher(binding.etLocationProfile,
                binding.tilLocationProfile, "Enter location", CustomWatcher.EditTextType.FirstName)
        )
    }

    /**
     * Below permission methods are override by PermissionActivity
     * If permissions are granted than picture dialog appear
     * else show toast
     */
    override fun onPermissionGranted(REQUESTED_FOR: Int) {
        showPictureAndCameraPopup()
    }

    override fun onPermissionDisabled(REQUESTED_FOR: Int) {
        Toaster.shortToast("Please enable permission from app settings")
    }

    override fun onPermissionDenied(REQUESTED_FOR: Int) {
        Toaster.shortToast("Please allow STORAGE | CAMERA permission to update profile pic.")
    }

    /** Check @Internet Status */
    override fun onStart() {
        registerReceiver(neTWorkChange, IntentFilter(ConnectivityManager.CONNECTIVITY_ACTION))
        super.onStart()
    }

    /** Check @Internet Status */
    override fun onStop() {
        unregisterReceiver(neTWorkChange);
        super.onStop()
    }
}