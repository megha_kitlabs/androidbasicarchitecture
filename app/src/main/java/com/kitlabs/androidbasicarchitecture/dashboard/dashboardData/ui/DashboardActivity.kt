package com.kitlabs.androidbasicarchitecture.dashboard.dashboardData.ui

import android.content.Intent
import android.content.IntentFilter
import android.net.ConnectivityManager
import android.os.Bundle
import android.widget.ExpandableListAdapter
import android.widget.ImageView
import android.widget.RelativeLayout
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity
import androidx.core.view.GravityCompat
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import com.bumptech.glide.Glide
import com.google.android.material.appbar.MaterialToolbar
import com.google.android.material.navigation.NavigationView
import com.kitlabs.androidbasicarchitecture.R
import com.kitlabs.androidbasicarchitecture.base.SharedPref
import com.kitlabs.androidbasicarchitecture.dashboard.changePassword.ui.ChangePasswordActivity
import com.kitlabs.androidbasicarchitecture.dashboard.dashboardData.adapter.ExpandableListAdapterr
import com.kitlabs.androidbasicarchitecture.dashboard.dashboardData.model.ExpandedMenuModel
import com.kitlabs.androidbasicarchitecture.dashboard.dashboardData.viewmodel.DashboardVM
import com.kitlabs.androidbasicarchitecture.dashboard.notification.NotificationActivity
import com.kitlabs.androidbasicarchitecture.dashboard.updateProfile.ui.UpdateProfileActivity
import com.kitlabs.androidbasicarchitecture.databinding.ActivityDashboardBinding
import com.kitlabs.androidbasicarchitecture.membership.ui.MembershipActivity
import com.kitlabs.androidbasicarchitecture.membership.ui.PurchasedMembershipInfoActivity
import com.kitlabs.androidbasicarchitecture.others.Cons
import com.kitlabs.androidbasicarchitecture.others.NeTWorkChange
import com.kitlabs.androidbasicarchitecture.others.Toaster
import com.kitlabs.androidbasicarchitecture.userAction.UserActivity

class DashboardActivity : AppCompatActivity() {

    lateinit var binding: ActivityDashboardBinding
    var viewModel: DashboardVM = DashboardVM()
    private lateinit var adapter: ExpandableListAdapter
    private val listDataHeader = ArrayList<ExpandedMenuModel>()
    private lateinit var toolbar: MaterialToolbar
    private lateinit var ivEdit: RelativeLayout
    private lateinit var tvName: TextView
    private lateinit var tvAbout: TextView
    private lateinit var profileImage: ImageView
    private lateinit var ivNotification: ImageView
    private val neTWorkChange = NeTWorkChange()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView(this, R.layout.activity_dashboard)
        binding.viewModel = viewModel

        setReferences()
        setDrawerData()

    }

    /**
     * Set the Navigation Drawer adapter
     * Open screens accoring to drawer elements
     */
    private fun setDrawerData() {
        prepareListData()
        adapter = ExpandableListAdapterr(listDataHeader)
        binding.expandableLV.setAdapter(adapter)
        binding.expandableLV.setOnGroupClickListener { expandableListView, view, i, l ->
            when (i) {
                1 -> {
                    startActivity(Intent(this, ChangePasswordActivity::class.java))
                }
                2 -> {
                    startActivity(Intent(this, MembershipActivity::class.java))
                }
                3 -> {
                    startActivity(Intent(this, PurchasedMembershipInfoActivity::class.java))
                }
                4 -> {
                    SharedPref.get().clearAll()
                    this.startActivity(Intent(this, UserActivity::class.java))
                    this.finishAffinity()
                }
            }
            toggleDrawer()
            false
        }
    }

    /**
     * Bind the views and handle clicks
     * If the response is success than set the data in views of Navigation drawer
     */
    private fun setReferences() {
        toolbar = findViewById(R.id.toolbar)
        ivEdit = findViewById(R.id.layoutEdit)
        tvName = findViewById(R.id.tvNameNavDrawer)
        tvAbout = findViewById(R.id.tvAboutNavDrawer)
        profileImage = findViewById(R.id.profileImageNavDrawer)
        ivNotification = findViewById(R.id.icNotification)
        setupDrawerContent(binding.navView)
        toolbar.title = "Dashboard"
        toolbar.isTitleCentered = true
        setSupportActionBar(toolbar)

        /** Set Navigation drawer and user details */
        toolbar.setNavigationOnClickListener {
            setNavUserDetails()
            toggleDrawer()
        }

        /** @Edit User Profile click listener */
        ivEdit.setOnClickListener {
            startActivity(Intent(this, UpdateProfileActivity::class.java))
            toggleDrawer()
        }

        /** @Notification click listener */
        ivNotification.setOnClickListener {
            startActivity(Intent(this, NotificationActivity::class.java))
        }

    }

    /**
     * observe API Status
     * If the response is success than set the data in views of Navigation drawer
     */
    private fun setNavUserDetails() {
        viewModel.getUserDetails()
        viewModel.getUserLiveData?.observe(this, Observer {
            if (it != null) {
                if (it.status == "success") {
                    tvName.text = it.data.name
                    tvAbout.text = it.data.aboutUser
                    //PicassoUtil.loadProfileImage(profileImage, it.data.imagePath)
                    Glide.with(this)
                        .load(Cons.IMAGE_URL + it.data.imagePath)
                        .error(R.drawable.error_image)
                        .into(profileImage)
                }
                else {
                    Toaster.shortToast(it.message)
                }
            }
        })

        viewModel.errorLiveData.observe(this, Observer {
            Toaster.shortToast(it)
        })
    }

    /** Adding Navigation Drawer Elements and Icons in list */
    private fun prepareListData() {
        listDataHeader.add(ExpandedMenuModel("Home", R.drawable.ic_home))
        listDataHeader.add(ExpandedMenuModel("Change Password", R.drawable.ic_lock))
        listDataHeader.add(ExpandedMenuModel("Membership Plans", R.drawable.ic_membership))
        listDataHeader.add(ExpandedMenuModel("Membership Info", R.drawable.ic_info))
        listDataHeader.add(ExpandedMenuModel("Logout", R.drawable.ic_logout))
    }

    /** For Opening and Closing of Navigation Drawer */
    private fun toggleDrawer() {
        if (binding.drawerLayout.isDrawerOpen(GravityCompat.START)) {
            binding.drawerLayout.closeDrawer(GravityCompat.START)
        } else {
            binding.drawerLayout.openDrawer(GravityCompat.START)
        }
    }

    private fun setupDrawerContent(navigationView: NavigationView) {
        navigationView.setNavigationItemSelectedListener { menuItem ->
            menuItem.isChecked = true
            binding.drawerLayout.closeDrawers()
            true
        }
    }

    /** Check @Internet Status */
    override fun onStart() {
        registerReceiver(neTWorkChange, IntentFilter(ConnectivityManager.CONNECTIVITY_ACTION))
        super.onStart()
    }

    /** Check @Internet Status */
    override fun onStop() {
        unregisterReceiver(neTWorkChange);
        super.onStop()
    }
}