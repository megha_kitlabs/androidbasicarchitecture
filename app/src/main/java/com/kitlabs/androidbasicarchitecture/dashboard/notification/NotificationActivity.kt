package com.kitlabs.androidbasicarchitecture.dashboard.notification

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.databinding.DataBindingUtil
import com.google.firebase.messaging.FirebaseMessaging
import com.kitlabs.androidbasicarchitecture.R
import com.kitlabs.androidbasicarchitecture.databinding.ActivityNotificationBinding

class NotificationActivity : AppCompatActivity() {

    lateinit var binding : ActivityNotificationBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView(this, R.layout.activity_notification)

        setClicks()
    }

    /** for handling clicks */
    private fun setClicks() {

        /** Button @Back click listener */
        binding.buttonBack.setOnClickListener {
            onBackPressed()
        }
    }
}