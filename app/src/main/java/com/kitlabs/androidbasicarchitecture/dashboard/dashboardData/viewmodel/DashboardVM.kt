package com.kitlabs.androidbasicarchitecture.dashboard.dashboardData.viewmodel

import android.util.Log
import androidx.databinding.BaseObservable
import androidx.lifecycle.MutableLiveData
import com.google.gson.Gson
import com.google.gson.JsonObject
import com.kitlabs.androidbasicarchitecture.dashboard.updateProfile.model.UserDetailBean
import com.kitlabs.androidbasicarchitecture.network.RetrofitApi
import com.kitlabs.androidbasicarchitecture.network.RetrofitClient
import com.kitlabs.androidbasicarchitecture.others.Toaster
import org.json.JSONObject
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class DashboardVM : BaseObservable(){

    /** Initialize API Response. */
    var getUserLiveData: MutableLiveData<UserDetailBean>? = null
    var errorLiveData = MutableLiveData<String>()

    init {
        getUserLiveData = MutableLiveData<UserDetailBean>()
        errorLiveData = MutableLiveData<String>()
    }

    /** API for get User Details for navigation drawer */
    fun getUserDetails() {
        val userDetails = RetrofitClient.getUserDetails().create(RetrofitApi::class.java)
        userDetails.getUserDetails().enqueue(object : Callback<JsonObject> {
            override fun onResponse(call: Call<JsonObject>, response: Response<JsonObject>) {
                try{
                    if (response.code() == 200 && response.isSuccessful){
                        val userDetail = Gson().fromJson(response.body(), UserDetailBean::class.java)
                        getUserLiveData?.value = userDetail
                    }
                    else{
                        val jObjError = response.errorBody()?.string()?.let { JSONObject(it) }
                        Toaster.shortToast(jObjError?.getString("message").toString())
                    }
                } catch (e : Exception){
                    Log.e("OnException", e.localizedMessage)
                }
            }

            override fun onFailure(call: Call<JsonObject>, t: Throwable) {
                Log.e("OnFailure", t.localizedMessage)
            }

        })
    }
}