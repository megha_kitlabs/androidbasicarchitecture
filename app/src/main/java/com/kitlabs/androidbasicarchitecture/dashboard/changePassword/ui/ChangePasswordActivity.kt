package com.kitlabs.androidbasicarchitecture.dashboard.changePassword.ui

import android.content.IntentFilter
import android.net.ConnectivityManager
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import com.kitlabs.androidbasicarchitecture.R
import com.kitlabs.androidbasicarchitecture.dashboard.changePassword.viewmodel.ChangePasswordVM
import com.kitlabs.androidbasicarchitecture.databinding.ActivityChangePasswordBinding
import com.kitlabs.androidbasicarchitecture.others.CustomWatcher
import com.kitlabs.androidbasicarchitecture.others.NeTWorkChange
import com.kitlabs.androidbasicarchitecture.others.Toaster

class ChangePasswordActivity : AppCompatActivity() {

    lateinit var binding :ActivityChangePasswordBinding
    var viewModel: ChangePasswordVM = ChangePasswordVM()
    private val neTWorkChange = NeTWorkChange()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView(this, R.layout.activity_change_password)
        binding.viewModel = viewModel
        validateInputType()

        setClicks()

        bindObserver()

    }

    /**
     * observe API Status
     * According to the response show a toast
     */
    private fun bindObserver() {
        viewModel.responseLiveData?.observe(this, Observer {
            if (it != null) {
                Toaster.shortToast(it.message)
            }
        })

        viewModel.errorLiveData.observe(this, Observer {
            Toaster.shortToast(it)
        })
    }

    /** for handling clicks */
    private fun setClicks() {

        /** @ChangePassword click listener */
        binding.buttonUpdate.setOnClickListener {
            viewModel.submitData(binding.tvUpdate, binding.loader)
        }

        /** Button @Back click listener */
        binding.buttonBack.setOnClickListener {
            onBackPressed()
        }
    }

    /** TextWatcher that watch a input text field and can instantly update data */
    private fun validateInputType() {
        binding.etCurrentPassword.addTextChangedListener(
            CustomWatcher(binding.etCurrentPassword,
                binding.tilCurrentPassword, "Enter current password", CustomWatcher.EditTextType.Password)
        )

        binding.etNewPassword.addTextChangedListener(
            CustomWatcher(binding.etNewPassword,
                binding.tilNewPassword, "Enter new password", CustomWatcher.EditTextType.Password)
        )

        binding.etConfirmPassword.addTextChangedListener(
            CustomWatcher(binding.etConfirmPassword,
                binding.tilConfirmPassword, "Enter confirm password", CustomWatcher.EditTextType.Password)
        )
    }

    override fun onStart() {
        registerReceiver(neTWorkChange, IntentFilter(ConnectivityManager.CONNECTIVITY_ACTION))
        super.onStart()
    }
    override fun onStop() {
        unregisterReceiver(neTWorkChange);
        super.onStop()
    }
}