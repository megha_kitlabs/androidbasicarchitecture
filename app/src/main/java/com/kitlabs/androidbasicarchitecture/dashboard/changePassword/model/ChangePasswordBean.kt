package com.kitlabs.androidbasicarchitecture.dashboard.changePassword.model


import com.google.gson.annotations.SerializedName

data class ChangePasswordBean(
    @SerializedName("status")
    val status: String,
    @SerializedName("message")
    val message: String
)