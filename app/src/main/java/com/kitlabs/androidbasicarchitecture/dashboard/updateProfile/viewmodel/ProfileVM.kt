package com.kitlabs.androidbasicarchitecture.dashboard.updateProfile.viewmodel

import android.content.Context
import android.util.Log
import android.widget.TextView
import androidx.databinding.BaseObservable
import androidx.databinding.ObservableField
import androidx.lifecycle.MutableLiveData
import com.airbnb.lottie.LottieAnimationView
import com.bumptech.glide.Glide
import com.google.gson.Gson
import com.google.gson.JsonObject
import com.kitlabs.androidbasicarchitecture.R
import com.kitlabs.androidbasicarchitecture.base.BaseModel
import com.kitlabs.androidbasicarchitecture.dashboard.updateProfile.model.UpdateBean
import com.kitlabs.androidbasicarchitecture.dashboard.updateProfile.model.UserDetailBean
import com.kitlabs.androidbasicarchitecture.network.RetrofitApi
import com.kitlabs.androidbasicarchitecture.network.RetrofitClient
import com.kitlabs.androidbasicarchitecture.others.*
import de.hdodenhof.circleimageview.CircleImageView
import okhttp3.MediaType
import okhttp3.MultipartBody
import okhttp3.RequestBody
import org.json.JSONObject
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.io.File

class ProfileVM : BaseObservable() {

    /** Initialize Text Field. */
    var firstName = ObservableField<String>()
    val lastName = ObservableField<String>()
    var email = ObservableField<String>()
    var phone = ObservableField<String>()
    var location = ObservableField<String>()
    var about = ObservableField<String>()

    /** Initialize Validate Fields. */
    var isValidFName = ObservableField(BaseModel(true))
    var isValidLName = ObservableField(BaseModel(true))
    var isValidLocation = ObservableField(BaseModel(true))

    /** Initialize API Response. */
    var getUserLiveData: MutableLiveData<UserDetailBean>? = null
    var updateLiveData: MutableLiveData<UpdateBean>? = null
    var errorLiveData = MutableLiveData<String>()

    init {
        getUserLiveData = MutableLiveData<UserDetailBean>()
        updateLiveData = MutableLiveData<UpdateBean>()
        errorLiveData = MutableLiveData<String>()
    }

    /** API for get User Details */
    fun getUserDetails(context: Context, profileImage: CircleImageView) {
        val loader = Loader(context)
        loader.show()
        val userDetails = RetrofitClient.getUserDetails().create(RetrofitApi::class.java)
        userDetails.getUserDetails().enqueue(object : Callback<JsonObject> {
            override fun onResponse(call: Call<JsonObject>, response: Response<JsonObject>) {
                try{
                    if (response.code() == 200 && response.isSuccessful){
                        val userDetail = Gson().fromJson(response.body(), UserDetailBean::class.java)
                        getUserLiveData?.value = userDetail
                        setData(context, userDetail.data, profileImage)
                    }
                    else{
                        val jObjError = response.errorBody()?.string()?.let { JSONObject(it) }
                        Toaster.shortToast(jObjError?.getString("message").toString())
                    }
                } catch (e : Exception){
                    Log.e("OnException", e.localizedMessage)
                }
                loader.dismiss()
            }

            override fun onFailure(call: Call<JsonObject>, t: Throwable) {
                Log.e("OnFailure", t.localizedMessage)
                loader.dismiss()
            }

        })
    }

    /** Set the user data in input fields */
    private fun setData(
        context: Context,
        userDetail: UserDetailBean.Data,
        profileImage: CircleImageView
    ) {
        firstName.set(CaseSensitiveHelper.returnCamelCaseWord(userDetail.firstName))
        lastName.set(CaseSensitiveHelper.returnCamelCaseWord(userDetail.lastName))
        email.set(userDetail.email)
        phone.set(userDetail.phone)
        location.set(userDetail.location)
        about.set(userDetail.aboutUser)
        Glide.with(context)
            .load(Cons.IMAGE_URL + userDetail.imagePath)
            .error(R.drawable.error_image)
            .into(profileImage)
        //PicassoUtil.loadProfileImage(profileImage, userDetail.imagePath)
    }

    /**
     * On button click
     * @UpdateProfile it will check first validations.
     * If all fields valid then Go for Api Access and update the user profile.
     * */
    fun updateData(fil: File?, tvSend: TextView, loader: LottieAnimationView) {
        if (checkValidation()) {
            MyUtils.viewGone(tvSend)
            MyUtils.viewVisible(loader)
            val builder = MultipartBody.Builder()
            builder.setType(MultipartBody.FORM)
            builder.addFormDataPart("firstname", CaseSensitiveHelper.returnCamelCaseWord(firstName.get().toString().trim()))
            builder.addFormDataPart("lastname", CaseSensitiveHelper.returnCamelCaseWord(lastName.get().toString().trim()))
            builder.addFormDataPart("phone", location.get().toString().trim())
            builder.addFormDataPart("location", phone.get().toString().trim())
            builder.addFormDataPart("about_user", about.get().toString().trim())
            if (fil != null) {
            builder.addFormDataPart("image", fil.name,
                RequestBody.create(MediaType.parse("multipart/form-data"), fil))
            }

            val requestBody = builder.build()
            val updateRequest = RetrofitClient.getUserDetails().create(RetrofitApi::class.java)
            updateRequest.updateProfile(requestBody).enqueue(object : Callback<JsonObject>{
                override fun onResponse(call: Call<JsonObject>, response: Response<JsonObject>) {
                    try{
                        if (response.code() == 200 && response.isSuccessful){
                            val update = Gson().fromJson(response.body(), UpdateBean::class.java)
                            updateLiveData?.value = update
                        }
                        else{
                            val jObjError = response.errorBody()?.string()?.let { JSONObject(it) }
                            errorLiveData.value = jObjError?.getString("message")
                        }
                    } catch (e : Exception){
                        Log.e("OnException", e.localizedMessage)
                    }
                    MyUtils.viewGone(loader)
                    MyUtils.viewVisible(tvSend)
                }

                override fun onFailure(call: Call<JsonObject>, t: Throwable) {
                    Log.e("OnFailure", t.localizedMessage)
                    MyUtils.viewGone(loader)
                    MyUtils.viewVisible(tvSend)
                }

            })
        }
    }

    /** Check Validation
     * If all fields valid then return true
     * */
    private fun checkValidation(): Boolean {
        var valid = false
        when {
            MyUtils.isEmptyString(firstName.get()) -> {
                isValidFName.set(BaseModel(message = "Enter first name"))
            }
            MyUtils.isEmptyString(lastName.get()) -> {
                isValidLName.set(BaseModel(message = "Enter last name"))
            }
            MyUtils.isEmptyString(location.get()) -> {
                isValidLocation.set(BaseModel(message = "Enter location"))
            }
            else -> {
                valid = true
            }
        }
        return valid
    }
}