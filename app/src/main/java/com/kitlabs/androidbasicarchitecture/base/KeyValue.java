package com.kitlabs.androidbasicarchitecture.base;

public class KeyValue {

    public static final String username = "user_login";
    public static final String firstName = "firstName";
    public static final String lastName = "lastName";
    public static final String user_email = "user_email";
    public static final String token = "token";
    public static final String logintype = "logintype";
    public static final String IS_MEMEBERSHIP_PURCHASE = "is_membership_purchased";
    public static final String phone="phone";
    public static final String countryCode= "countryCode";
    public static final String SUCCESS= "com.turnupwith.tanci.android.success";
    public static final String MembershipDetail="MembershipDetail";
    public static final String MEMBERSHIPID="MembershipiD";
    public static final String MEMBERSHIP_AMOUNT="MEMBERSHIP_AMOUNT";
    public static final String MEMBERSHIP_Frequency="MEMBERSHIP_Frequency";
    public static final String AMOUNT="AMOUNt";
    public static final int PAYMENT_RESPONSE=14566;
    public static final String TRANSACTIONID="TRANSACTIONID";
    public static final int DROP_IN_REQUEST = 1;
    public static final String MEMBERSHIP_END_DATE = "membership_Enddate";
    public static final String id= "id";
    public static final String image_path= "image_path";
}
