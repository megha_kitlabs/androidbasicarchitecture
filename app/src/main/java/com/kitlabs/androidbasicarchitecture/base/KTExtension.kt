package com.kitlabs.androidbasicarchitecture.base

import android.content.Context
import android.widget.ImageView
import androidx.swiperefreshlayout.widget.CircularProgressDrawable
import com.bumptech.glide.Glide
import com.bumptech.glide.request.RequestOptions
import com.kitlabs.androidbasicarchitecture.R
import com.kitlabs.androidbasicarchitecture.others.ResourceUtils


fun getProgressDrawable(context: Context): CircularProgressDrawable {
    return CircularProgressDrawable(context).apply {
        strokeWidth = 10f
        centerRadius = 25f
        setColorSchemeColors(ResourceUtils.getColor(R.color.teal_200))
        start()
    }
}

fun ImageView.loadImageWithLoader(uri: String?, progressDrawable: CircularProgressDrawable) {
    val options = RequestOptions()
        .placeholder(progressDrawable)
        //.error(R.drawable.ic_baseline_person_outline_24)
    Glide.with(context.applicationContext).setDefaultRequestOptions(options)
        .load(uri)
        .into(this)

}
