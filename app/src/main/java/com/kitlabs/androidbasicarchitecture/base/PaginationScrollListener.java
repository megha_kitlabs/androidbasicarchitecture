package com.kitlabs.androidbasicarchitecture.base;

import android.util.Log;

import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

public abstract class PaginationScrollListener extends RecyclerView.OnScrollListener {

    private GridLayoutManager layoutManager;

    public PaginationScrollListener(GridLayoutManager layoutManager) {
        this.layoutManager = layoutManager;
    }

    @Override
    public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
        super.onScrolled(recyclerView, dx, dy);

        int visibleItemCount = layoutManager.getChildCount();
        int totalItemCount = layoutManager.getItemCount();
        int firstVisibleItemPosition = layoutManager.findLastVisibleItemPosition();
        Log.e("onScrolled: ","isLoading "+isLoading()+"isLastPAge"+isLastPage()+"visible "+firstVisibleItemPosition+" total "+totalItemCount );
        if (!isLoading() && !isLastPage()) {
           // if (firstVisibleItemPosition > totalItemCount - 3
            if (visibleItemCount + firstVisibleItemPosition >= totalItemCount && firstVisibleItemPosition >= 0)
                // if ((visibleItemCount + firstVisibleItemPosition) >=
             {
                loadMoreItems();
            }
        }
    }

    protected abstract void loadMoreItems();

    public abstract boolean isLastPage();

    public abstract boolean isLoading();

}