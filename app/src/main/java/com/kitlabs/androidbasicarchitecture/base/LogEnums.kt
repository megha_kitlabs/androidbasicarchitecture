package com.kitlabs.androidbasicarchitecture.base

object LogEnums {
    enum class LogType {
       MyError, MyWarnings, MyInfo,MyDebug, MyVerbos
    }
}