package com.kitlabs.androidbasicarchitecture.membership.ui

import com.kitlabs.androidbasicarchitecture.base.App
import com.kitlabs.androidbasicarchitecture.base.BaseActivity
import com.kitlabs.androidbasicarchitecture.membership.helpers.DataCache
import com.kitlabs.androidbasicarchitecture.membership.helpers.MembershipRequest
import com.kitlabs.androidbasicarchitecture.membership.model.UserMembershipKBean
import com.kitlabs.androidbasicarchitecture.others.CallBack

abstract class MembershipActivityCheck : BaseActivity() {

    private val TAG = "MembershipActivityCheck"
    override fun onStart() {
        super.onStart()
        if (!DataCache.get().isPreviouslyMembershipChecked && App.get().isConnected()) {
            checkMembershipData()
        }

    }

    private fun checkMembershipData() {

        MembershipRequest.checkMembership(object : CallBack<UserMembershipKBean.Data>() {
            override fun onSuccess(data: UserMembershipKBean.Data?) {
                if (data == null) {
                    NoMembership(this@MembershipActivityCheck).show()
                } else if (data.isValid=="false") {
                        MembershipRequest.showMembershipExpiredDialog(this@MembershipActivityCheck, data)
                    }
            }

            override fun onError(error: String?) {
                super.onError(error)
            }
        })
    }







}