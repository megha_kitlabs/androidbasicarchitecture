package com.kitlabs.androidbasicarchitecture.membership.ui

import android.app.Activity
import android.content.Intent
import android.content.IntentFilter
import android.net.ConnectivityManager
import android.os.Bundle
import android.util.Log
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import com.kitlabs.androidbasicarchitecture.R
import com.kitlabs.androidbasicarchitecture.base.KeyValue
import com.kitlabs.androidbasicarchitecture.base.LogEnums
import com.kitlabs.androidbasicarchitecture.base.MyLogs
import com.kitlabs.androidbasicarchitecture.base.SharedPref
import com.kitlabs.androidbasicarchitecture.databinding.ActivityMembershipBinding
import com.kitlabs.androidbasicarchitecture.membership.adapter.MembershipAdapter
import com.kitlabs.androidbasicarchitecture.membership.helpers.GooglePlayActivity
import com.kitlabs.androidbasicarchitecture.membership.model.MembershipBean
import com.kitlabs.androidbasicarchitecture.membership.viewmodel.MembershipVM
import com.kitlabs.androidbasicarchitecture.others.CallBack
import com.kitlabs.androidbasicarchitecture.others.NeTWorkChange
import com.kitlabs.androidbasicarchitecture.others.Toaster
import com.kitlabs.androidbasicarchitecture.others.MyUtils
import com.kitlabs.androidbasicarchitecture.userAction.UserActivity

class MembershipActivity : GooglePlayActivity() {

    lateinit var binding : ActivityMembershipBinding
    var viewModel: MembershipVM = MembershipVM()
    private val neTWorkChange = NeTWorkChange()
    var selectedPosition: Int = -1
    lateinit var membershipBean: MembershipBean
    lateinit var mBeans: MembershipBean.Data

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView(this, R.layout.activity_membership)
        binding.viewModel = viewModel

        viewModel.getMembership(this)

        setClicks()
        bindObserver()
    }

    private fun bindObserver() {
        viewModel.getMembershipLiveData?.observe(this, Observer {
            val listSize = it.data.size
            membershipBean = it
            when {
                it == null -> {
                    MyLogs.addLog(LogEnums.LogType.MyInfo, "GetMembership",
                        "Empty membership #response.body() ")
                    return@Observer
                }
                listSize == 0 -> {
                    MyLogs.addLog(LogEnums.LogType.MyInfo, "GetMembership", "There's no membership available ")
                    MyUtils.viewVisible(binding.tvNoItems)
                    MyUtils.viewGone(binding.rvMembership)
                }
                else -> {
                    try {
                        binding.rvMembership.adapter = MembershipAdapter(membershipBean.data ,object : CallBack<Int>() {
                            override fun onSuccess(position: Int) {
                                selectedPosition = position
                                mBeans = membershipBean.data[selectedPosition]

                                ConfirmMembershipDialog(this@MembershipActivity, mBeans.price, object : CallBack<Int>() {
                                    override fun onSuccess(t: Int?) {
                                        if (t == 1) {
                                            currentSelectedMembershipData = mBeans
                                            setupGooglePlayFlow(mBeans)

                                        }
                                    }
                                }).show()
                            }

                        })

                        Log.d("sdasdad", "sdasjldjakdja: " + membershipBean.data.size)
                        MyLogs.addLog(LogEnums.LogType.MyInfo,
                            "Listed membership: ", "Available membership size: " + membershipBean.data.size)

                    } catch (e: Exception) {
                        Log.e("onException", e.localizedMessage)
                    }
                }
            }
        })

        viewModel.errorLiveData.observe(this, Observer {
            Toaster.shortToast(it)
        })
    }

    private fun setClicks() {

        binding.buttonLogout.setOnClickListener {
            MyLogs.addLog(LogEnums.LogType.MyInfo,
                "MembershipActivity button #: Logout",
                "User logout from membership activity without purchase!")
            SharedPref.get().clearAll()
            this.startActivity(Intent(this, UserActivity::class.java))
            this.finishAffinity()
        }

        binding.buttonBack.setOnClickListener {
            onBackPressed()
        }

    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == KeyValue.PAYMENT_RESPONSE && resultCode == Activity.RESULT_OK && data != null) {

            val isSuccess = data.getBooleanExtra(KeyValue.SUCCESS, false)
            if (isSuccess) {
                val transactionId = data.getStringExtra(KeyValue.TRANSACTIONID)
                val memberShipid = data.getIntExtra(KeyValue.MEMBERSHIPID, 0).toString()
                val amount = data.getStringExtra(KeyValue.AMOUNT)
                val frequency = data.getStringExtra(KeyValue.MEMBERSHIP_Frequency)
                viewModel.purchaseMembership(this, transactionId, memberShipid, amount, frequency)
            }
        }
    }

    override fun onStart() {
        registerReceiver(neTWorkChange, IntentFilter(ConnectivityManager.CONNECTIVITY_ACTION))
        super.onStart()
    }
    override fun onStop() {
        unregisterReceiver(neTWorkChange);
        super.onStop()
    }
}