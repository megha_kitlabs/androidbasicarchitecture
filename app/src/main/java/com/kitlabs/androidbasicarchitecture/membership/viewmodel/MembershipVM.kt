package com.kitlabs.androidbasicarchitecture.membership.viewmodel

import android.content.Context
import android.util.Log
import androidx.lifecycle.MutableLiveData
import com.google.gson.Gson
import com.google.gson.JsonObject
import com.kitlabs.androidbasicarchitecture.base.KeyValue
import com.kitlabs.androidbasicarchitecture.base.LogEnums
import com.kitlabs.androidbasicarchitecture.base.MyLogs
import com.kitlabs.androidbasicarchitecture.base.SharedPref
import com.kitlabs.androidbasicarchitecture.membership.model.BuyMembershipBean
import com.kitlabs.androidbasicarchitecture.membership.model.MembershipBean
import com.kitlabs.androidbasicarchitecture.network.RetrofitApi
import com.kitlabs.androidbasicarchitecture.network.RetrofitClient
import com.kitlabs.androidbasicarchitecture.others.ErrorUtils
import com.kitlabs.androidbasicarchitecture.others.Loader
import com.kitlabs.androidbasicarchitecture.others.Toaster
import com.kitlabs.androidbasicarchitecture.others.MyUtils
import org.json.JSONObject
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.text.SimpleDateFormat

/**
 * Save end date of membership in Shared Pref in purchaseMembership method.
 **/
class MembershipVM {

    var TAG = "MembershipVM"
    var endDate = SimpleDateFormat("yyyy-MM-dd HH:mm:ss")
    var getMembershipLiveData: MutableLiveData<MembershipBean>? = null
    var errorLiveData = MutableLiveData<String>()

    init {
        getMembershipLiveData = MutableLiveData<MembershipBean>()
        errorLiveData = MutableLiveData<String>()
    }

    fun getMembership(context: Context) {
        val loader = Loader(context)
        loader.show()
        val membership = RetrofitClient.getUserDetails().create(RetrofitApi::class.java)
        membership.getMembership("https://private-d1eb04-membership9.apiary-mock.com/get_membership").enqueue(object : Callback<MembershipBean> {
            override fun onResponse(call: Call<MembershipBean>, response: Response<MembershipBean>) {
                try{
                    if (response.code() == 200 && response.isSuccessful){
                        getMembershipLiveData?.value = response.body()
                    }
                    else{
                        val jObjError = response.errorBody()?.string()?.let { JSONObject(it) }
                        Toaster.shortToast(jObjError?.getString("message").toString())
                        MyLogs.addLog(LogEnums.LogType.MyError, "onError in response ",
                            "Something went wrong in membership API")
                    }
                } catch (e : Exception){
                    Log.e("OnException", e.localizedMessage)
                }
                loader.dismiss()
            }

            override fun onFailure(call: Call<MembershipBean>, t: Throwable) {
                Log.e("OnFailure", t.localizedMessage)
                loader.dismiss()
            }

        })
    }

    fun purchaseMembership(context: Context, transactionId: String?, memberShipID: String, amount: String?, frequency: String?) {
        val loader = Loader(context)
        try {
            val sharedPref = SharedPref(context)
            val hashMap = HashMap<String, String?>()
            if (!MyUtils.isEmptyString(transactionId) && !MyUtils.isEmptyString(memberShipID)) {
                hashMap["transactionNo"] = transactionId.toString()
                hashMap["membership_id"] = memberShipID
                hashMap["membership_frequency"] = frequency
                hashMap["amountPaid"] = amount.toString()
            }
            MyLogs.addLog(LogEnums.LogType.MyInfo, "Purchase membership Details", "Purchase membership details while purchasing: \n $memberShipID \n ${amount.toString()}")

            loader.show()

            val callPurchase = RetrofitClient.getUserDetails().create(RetrofitApi::class.java)
            callPurchase.purchaseMemberShip(hashMap).enqueue(object : Callback<JsonObject> {

                override fun onResponse(call: Call<JsonObject>, response: Response<JsonObject>) {
                    if (response.code() == 200 && response.isSuccessful) {
                        var bean = Gson().fromJson(response.body(), BuyMembershipBean::class.java)
                        sharedPref.save(KeyValue.IS_MEMEBERSHIP_PURCHASE, true)
                        sharedPref.save(KeyValue.MEMBERSHIP_END_DATE, "Save End date of membership here and remove this string"
                            /* endDate.parse(bean.data.membershipEndDate).time */)
                        MyLogs.addLog(LogEnums.LogType.MyInfo, "Membership purchase successfully", "Membership purchased successful ")

                        Toaster.shortToast("Membership Purchased Successfully")
                        /* val it = Intent(this@MembershipActivity, Dashboard::class.java)
                         intent.extras?.let { it1 -> it.putExtras(it1) }
                         startActivity(it)
                         finish()*/
                    } else {
                        MyLogs.addLog(
                            LogEnums.LogType.MyError,
                            "Error while purchasing  membership #MembershipActivity",
                            "Something went wrong while purchasing membership! "
                        )
                        ErrorUtils.parseError(response.errorBody()?.string())
                    }
                    loader.dismiss()
                }

                override fun onFailure(call: Call<JsonObject>, t: Throwable) {
                    loader.dismiss()
                    ErrorUtils.onFailure(t)
                    Log.e(TAG, "on failure ${t.message}")
                    MyLogs.addLog(LogEnums.LogType.MyError, "Purchase membership failed", "Something went wrong while purchasing membership! ")
                }
            })
        } catch (e: Exception) {
            loader.dismiss()
            Log.e(TAG, e.toString())
            Toaster.somethingWentWrong()
            MyLogs.addLog(LogEnums.LogType.MyError, "Error while initiating  membership #MembershipActivity",
                "Something went wrong while purchasing membership! ")
        }
    }

}