package com.kitlabs.androidbasicarchitecture.membership.helpers

import android.content.Intent
import android.os.Bundle
import android.util.Log
import com.android.billingclient.api.*
import com.kitlabs.androidbasicarchitecture.base.*
import com.kitlabs.androidbasicarchitecture.dashboard.dashboardData.ui.DashboardActivity
import com.kitlabs.androidbasicarchitecture.membership.adapter.MembershipAdapter
import com.kitlabs.androidbasicarchitecture.membership.model.MembershipBean
import com.kitlabs.androidbasicarchitecture.others.Toaster
import java.lang.StringBuilder

open class GooglePlayActivity : BaseActivity(), PurchasesUpdatedListener, PurchaseHistoryResponseListener {

    lateinit var billingClient: BillingClient
    var membershipAdapter: MembershipAdapter? = null
    var currentSelectedMembershipData: MembershipBean.Data? = null
    var currentCurrency: String? = null
    var price: String? = null
    var bundle = Bundle()
    val isBuilderBuilt = false
    private val TAG = "GooglePlayActivity"
    private var skuDetails: SkuDetails? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        try {
            billingClient = BillingClient.newBuilder(this).enablePendingPurchases().setListener(this).build()
            billingClient.startConnection(object : BillingClientStateListener {
                override fun onBillingSetupFinished(billingResult: BillingResult) {
                    billingClient.queryPurchaseHistoryAsync(
                        BillingClient.SkuType.SUBS,
                        this@GooglePlayActivity
                    )
                }
                override fun onBillingServiceDisconnected() {
                }
            })

        }
        catch (e: Exception) {

        }
    }


    override fun onPurchasesUpdated(billingResult: BillingResult, purchaseList: MutableList<Purchase>?, ) {
        try {
            when (billingResult.responseCode) {
                BillingClient.BillingResponseCode.OK -> {
                    if (purchaseList != null) {
                        for (purchase in purchaseList) {
                            showLoader()
                            acknowledgePurchase(purchase.purchaseToken)
                            PaymentApiRepository.saveInAppPayment(
                                currentSelectedMembershipData?.id,
                                purchase.orderId,
                                price?.toFloat(),
                                currentCurrency,
                                purchase.originalJson,
                                purchase.purchaseToken, paymentCallback
                            )
                        }
                    }

                }
                BillingClient.BillingResponseCode.ITEM_ALREADY_OWNED -> {
                    Toaster.shortToast("Already Purchased this subscription.")
                    MyLogs.addLog(LogEnums.LogType.MyWarnings, "BillingResponseCode #: GooglePlayActivity", "Already Purchased this subscription.")
                }
                BillingClient.BillingResponseCode.USER_CANCELED -> {
                    /*Toaster.shortToast("You've cancelled the Google play billing process...")*/
                    MyLogs.addLog(LogEnums.LogType.MyWarnings, "BillingResponseCode.USER_CANCELED #: GooglePlayActivity", "Membership cancelled by user!")
                }

                BillingClient.BillingResponseCode.SERVICE_DISCONNECTED -> {
                    /*Toaster.shortToast("You've cancelled the Google play billing process...")*/

                    MyLogs.addLog(
                        LogEnums.LogType.MyWarnings,
                        "BillingResponseCode.SERVICE_DISCONNECTED #: GooglePlayActivity",
                        "Google play billing process...! cancelled by user"
                    )

                }
                BillingClient.BillingResponseCode.SERVICE_UNAVAILABLE -> {
                    Log.e(TAG, "onPurchasesUpdated: ${billingResult.debugMessage}")
                    Toaster.somethingWentWrong()
                    MyLogs.addLog(
                        LogEnums.LogType.MyWarnings,
                        "BillingResponseCode.SERVICE_UNAVAILABLE #: GooglePlayActivity",
                        "Google play billing service not available for now"
                    )

                }
                else -> {
                    MyLogs.addLog(
                        LogEnums.LogType.MyError,
                        "onError #: GooglePlayActivity",
                        "Error while purchasing membership!"
                    )

                }
            }

        } catch (e: Exception) {
            MyLogs.addLog(
                LogEnums.LogType.MyError,
                "Exception occurred #: GooglePlayActivity",
                "Unable to catch exception: ${e.localizedMessage}"
            )

        }
    }

    private val paymentCallback =
        object : RetrofitCallback<String, PaymentApiRepository.GooglePaymentApiType>() {
            override fun onMessage(
                t: String?,
                type: PaymentApiRepository.GooglePaymentApiType,
                success: Boolean,
            ) {
                hideLoader()
                when (type) {
                    PaymentApiRepository.GooglePaymentApiType.Initiate -> {
                        val flowParams = skuDetails?.let {
                            BillingFlowParams.newBuilder().setSkuDetails(it).build()
                        }

                        flowParams?.let { it1 ->
                            billingClient.launchBillingFlow(this@GooglePlayActivity, it1)
                        }
                    }

                    PaymentApiRepository.GooglePaymentApiType.Complete -> {
                        MyLogs.addLog(
                            LogEnums.LogType.MyInfo,
                            "membership purchase #: GooglePlayActivity",
                            "Membership purchased successfully!"
                        )

                        if (success) {
                            t?.let { Toaster.shortToast(it) }
                            /*if (currentSelectedMembershipData?.isGrouped==1){
                                val intent=
                                    Intent(this@GooglePlayActivity, GroupPlanActivity::class.java)
                                intent.putExtra(Cons.TOTAL_GROUP_MEMBERS,currentSelectedMembershipData?.noOfGroupedAccounts)
                                intent.putExtra(Cons.SUBSCR,currentSelectedMembershipData?.id)
                                startActivity(intent)
                                return
                            }*/
                        }
                        SharedPref.get().save(KeyValue.IS_MEMEBERSHIP_PURCHASE, true)
                        startActivity(
                            Intent(
                                this@GooglePlayActivity,
                                DashboardActivity::class.java
                            )
                        )
                        finishAffinity()

                        //  skipAction()
                    }
                }
            }


        }

    override fun onPurchaseHistoryResponse(
        p0: BillingResult,
        p1: MutableList<PurchaseHistoryRecord>?,
    ) {

    }

    private fun acknowledgePurchase(purchaseToken: String) {
        val params = AcknowledgePurchaseParams.newBuilder().setPurchaseToken(purchaseToken).build()
        billingClient.acknowledgePurchase(params) { billingResult ->
            if (billingResult.responseCode == BillingClient.BillingResponseCode.OK) {
                val debugMessage = billingResult.debugMessage
                // Toaster.shortToast("Item Purchased")

            }
        }
    }

    protected fun setupGooglePlayFlow(it: MembershipBean.Data) {
        try {
            if (it.googleActualPlanId.isNullOrEmpty()) {
                Toaster.shortToast("Error while purchasing this membership.")

                MyLogs.addLog(
                    LogEnums.LogType.MyInfo,
                    " google_actual_plan_id  #: GooglePlayActivity",
                    "Unable to find the google_actual_plan_id"
                )

                return
            }

            showLoader()

            Log.d("errrro", "reirureutoe$it")

            val skuList: MutableList<String> = ArrayList()
            skuList.add(it.googleActualPlanId)
            val params = SkuDetailsParams.newBuilder()
            params.setSkusList(skuList).setType(BillingClient.SkuType.SUBS)
            billingClient.querySkuDetailsAsync(params.build()) { _, p1 ->
                skuDetails = p1?.get(0)
                val priceAmount = skuDetails?.priceAmountMicros?.div(10000)
                price =
                    StringBuilder(priceAmount.toString()).insert(
                        priceAmount.toString().length - 2,
                        "."
                    )
                        .toString()

                currentCurrency = p1?.get(0)?.priceCurrencyCode
                if (currentSelectedMembershipData != null) {
                    MyLogs.addLog(
                        LogEnums.LogType.MyInfo,
                        " PaymentInitiated  #: GooglePlayActivity",
                        "Payment initiated:"
                    )

                    PaymentApiRepository.initiatePayment(
                        currentSelectedMembershipData?.id,
                        currentSelectedMembershipData?.googleActualPlanId,
                        skuDetails?.originalJson, paymentCallback
                    )

                } else {
                    hideLoader()

                    MyLogs.addLog(
                        LogEnums.LogType.MyError,
                        " error In payment initialisation  #: GooglePlayActivity",
                        "Unable to initialize the payment"
                    )
                }
                hideLoader()

            }

        } catch (e: Exception) {

            hideLoader()
            MyLogs.addLog(
                LogEnums.LogType.MyError,
                "Exception occurred #: GooglePlayActivity",
                "Unable to catch exception: ${e.localizedMessage}"
            )

        } finally {
            if (!isBuilderBuilt)
                hideLoader()
            Log.d("finally", "hjhhffl" + "finally")
            MyLogs.addLog(
                LogEnums.LogType.MyError,
                "onError #: GooglePlayActivity",
                "Unexpected error obscured!"
            )

        }
    }


}