package com.kitlabs.androidbasicarchitecture.membership.helpers

import com.google.gson.Gson
import com.google.gson.JsonObject
import com.kitlabs.androidbasicarchitecture.network.RetrofitApi
import com.kitlabs.androidbasicarchitecture.network.RetrofitClient
import com.kitlabs.androidbasicarchitecture.others.Cons
import com.kitlabs.androidbasicarchitecture.others.ErrorUtils
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.util.*
import kotlin.collections.HashMap

object PaymentApiRepository {
    /**Initiate the payment*/
    fun initiatePayment(
        membershipId: Int?,
        googlePlayActualPlan: String?,
        originalJsonObject: String?,
        callback: RetrofitCallback<String,GooglePaymentApiType>) {
        val map = HashMap<String, Any?>()
        map["membership_id"] = membershipId
        map["plan_id"] = googlePlayActualPlan
        map["platform"] = Cons.ANDROID
        map["type"] = "json"
        map["data_sent"] = originalJsonObject
        RetrofitClient.getUserDetails().create(RetrofitApi::class.java).initializePayment(map).enqueue(callback(callback,GooglePaymentApiType.Initiate))

    }

    /**Save In App Details in Server*/
    fun saveInAppPayment(
        membershipId: Int?,
        orderId: String?,
        pricePaid: Float?,
        currency: String?,
        originalJsonObject: String?,
        purchaseToken: String,
        callback: RetrofitCallback<String,GooglePaymentApiType>,
    ) {
        val map = HashMap<String, Any?>()
        map["membership_id"] = membershipId
        map["systemTransactionNo"] = UUID.randomUUID().toString()
        map["transaction_id"] = orderId
        map["payment_platform"] = Cons.GOOGLE_IN_APP
        map["pricePaid"] = pricePaid
        map["currency"] = currency
        map["completeResponseFrom"] = originalJsonObject
        map["purchase_token"] = purchaseToken
        RetrofitClient.getUserDetails().create(RetrofitApi::class.java).saveInAppPurchase(map).enqueue(callback(callback,GooglePaymentApiType.Complete))
    }
    /**Response from Api Management*/
    private fun callback(
        callback: RetrofitCallback<String, GooglePaymentApiType>,
        paymentTy: GooglePaymentApiType) = object : Callback<JsonObject?> {
            override fun onResponse(
                call: Call<JsonObject?>,
                response: Response<JsonObject?>,
            ) {
                val responseBody = Gson().fromJson(response.body(), RResponse::class.java)
                if (response.isSuccessful && response.code() == 200) {
                    callback.onMessage(responseBody.success, paymentTy,true)
                } else {
                    ErrorUtils.parseError(response.errorBody()?.string())
                    callback.onMessage(response.errorBody().toString(),paymentTy, false)

                }
            }

            override fun onFailure(call: Call<JsonObject?>, t: Throwable) {
                callback.onMessage(null, paymentTy,false)

            }
        }



    enum class GooglePaymentApiType {
        Initiate,
        Complete
    }
}




