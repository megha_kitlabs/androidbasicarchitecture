package com.kitlabs.androidbasicarchitecture.membership.ui

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.view.Window
import android.widget.RelativeLayout
import android.widget.TextView
import com.kitlabs.androidbasicarchitecture.R
import com.kitlabs.androidbasicarchitecture.base.BaseDialog
import com.kitlabs.androidbasicarchitecture.base.LogEnums
import com.kitlabs.androidbasicarchitecture.base.MyLogs
import com.kitlabs.androidbasicarchitecture.base.SharedPref
import com.kitlabs.androidbasicarchitecture.membership.helpers.DataCache
import com.kitlabs.androidbasicarchitecture.userAction.UserActivity


class NoMembership(val context: Activity) : BaseDialog(context) {
    override fun onCreate(savedInstanceState: Bundle?) {
        requestWindowFeature(Window.FEATURE_NO_TITLE)
        super.onCreate(savedInstanceState)
        setContentView(R.layout.layout_no_membership)
        val tvYes = findViewById<RelativeLayout>(R.id.buttonUpdate)
        val tvNo = findViewById<TextView>(R.id.buttonCancel)
        setDimBlur(window)
        setCancelable(false)
        tvYes.setOnClickListener {
            this@NoMembership.dismiss()

            MyLogs.addLog(
                LogEnums.LogType.MyInfo,
                "Opted to buy #: NoMembership",
                "User wants to purchase membership."
            )


            // TO redirect from NoMembership to Home Screen.......................
            context.startActivity(Intent(context, MembershipActivity::class.java))
            // context.startActivity(Intent(context, DashboardActivity::class.java))
        }

        tvNo.setOnClickListener {
            this@NoMembership.dismiss()
            MyLogs.addLog(
                LogEnums.LogType.MyInfo,
                "Opted to Cancel #: NoMembership",
                "User don't wants to purchase membership."
            )


            DataCache.get().clear()
            SharedPref.get().clearAll()
            context.startActivity(Intent(context, UserActivity::class.java))
            context.finishAffinity()
        }
    }
}