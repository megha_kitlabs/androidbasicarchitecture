package com.kitlabs.androidbasicarchitecture.membership.viewmodel

import android.content.Context
import android.util.Log
import androidx.lifecycle.MutableLiveData
import com.kitlabs.androidbasicarchitecture.base.LogEnums
import com.kitlabs.androidbasicarchitecture.base.MyLogs
import com.kitlabs.androidbasicarchitecture.membership.model.MembershipInfoBean
import com.kitlabs.androidbasicarchitecture.network.RetrofitApi
import com.kitlabs.androidbasicarchitecture.network.RetrofitClient
import com.kitlabs.androidbasicarchitecture.others.Loader
import com.kitlabs.androidbasicarchitecture.others.Toaster
import org.json.JSONObject
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class MembershipInfoVM {

    var getMembershipLiveData: MutableLiveData<MembershipInfoBean>? = null
    var errorLiveData = MutableLiveData<String>()

    init {
        getMembershipLiveData = MutableLiveData<MembershipInfoBean>()
        errorLiveData = MutableLiveData<String>()
    }

    fun getMembershipInfoData(context: Context) {
        val loader = Loader(context)
        loader.show()
        val membership = RetrofitClient.getUserDetails().create(RetrofitApi::class.java)
        membership.getMembershipInfo("https://private-d1eb04-membership9.apiary-mock.com/user_membership_plan")
            .enqueue(object : Callback<MembershipInfoBean> {
            override fun onResponse(call: Call<MembershipInfoBean>, response: Response<MembershipInfoBean>) {
                try{
                    if (response.code() == 200 && response.isSuccessful){
                        getMembershipLiveData?.value = response.body()
                    }
                    else{
                        val jObjError = response.errorBody()?.string()?.let { JSONObject(it) }
                        Toaster.shortToast(jObjError?.getString("message").toString())
                        MyLogs.addLog(
                            LogEnums.LogType.MyError, "onError in response ",
                            "Something went wrong in membership API")
                    }
                } catch (e : Exception){
                    Log.e("OnException", e.localizedMessage)
                }
                loader.dismiss()
            }

            override fun onFailure(call: Call<MembershipInfoBean>, t: Throwable) {
                Log.e("OnFailure", t.localizedMessage)
                loader.dismiss()
            }

        })
    }
}