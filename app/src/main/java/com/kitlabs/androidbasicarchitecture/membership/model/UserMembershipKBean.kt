package com.kitlabs.androidbasicarchitecture.membership.model

data class UserMembershipKBean(
    val `data`: Data,
    val message: String,
    val status: String
) {
    data class Data(
        val amountPaid: Double,
        val description: String,
        val isValid: String,
        val membership_frequency: String,
        val name: String,
        val status: String,
        val subscription_status: String,
        val trial_end_day: String,
        val trial_taken: Int,
        val validFrom: String,

        val validTo: String
    )
}