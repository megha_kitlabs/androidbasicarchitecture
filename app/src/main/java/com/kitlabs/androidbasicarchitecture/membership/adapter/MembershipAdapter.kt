package com.kitlabs.androidbasicarchitecture.membership.adapter

import android.annotation.SuppressLint
import android.content.Context
import android.text.Html
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.FrameLayout
import android.widget.RelativeLayout
import android.widget.TextView
import androidx.cardview.widget.CardView
import androidx.recyclerview.widget.RecyclerView
import com.kitlabs.androidbasicarchitecture.R
import com.kitlabs.androidbasicarchitecture.membership.model.MembershipBean
import com.kitlabs.androidbasicarchitecture.others.CallBack

class MembershipAdapter(private val list: List<MembershipBean.Data>, val callBack: CallBack<Int>)
    : RecyclerView.Adapter<MembershipAdapter.ViewHolder>(){

    var selectedPosition = -1
    lateinit var context: Context

    class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        val title: TextView = itemView.findViewById(R.id.title_membership)
        val price: TextView = itemView.findViewById(R.id.price_membership)
        val description: TextView = itemView.findViewById(R.id.description_membership)
        val buttonBuy: RelativeLayout = itemView.findViewById(R.id.buttonBuyNow)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        context = parent.context
        val v = LayoutInflater.from(parent.context).inflate(R.layout.item_membership, parent, false)
        return ViewHolder(v)
    }

    @SuppressLint("SetTextI18n")
    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val memberShip = list[position]

        holder.title.text = memberShip.name
        holder.price.text = "$ ${memberShip.price}"
        holder.description.text = Html.fromHtml(memberShip.description)

        holder.buttonBuy.setOnClickListener {
            selectedPosition = position;
            callBack.onSuccess(selectedPosition)
        }
    }

    override fun getItemCount(): Int {
        return list.size
    }

}