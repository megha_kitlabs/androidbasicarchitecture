package com.kitlabs.androidbasicarchitecture.membership.helpers


import android.app.Activity
import android.content.Intent
import com.google.gson.Gson
import com.google.gson.JsonObject
import com.kitlabs.androidbasicarchitecture.base.KeyValue
import com.kitlabs.androidbasicarchitecture.base.SharedPref
import com.kitlabs.androidbasicarchitecture.membership.model.UserMembershipKBean
import com.kitlabs.androidbasicarchitecture.membership.ui.MembershipActivity
import com.kitlabs.androidbasicarchitecture.membership.ui.MembershipExpiredDialog
import com.kitlabs.androidbasicarchitecture.network.RetrofitClient
import com.kitlabs.androidbasicarchitecture.others.CallBack
import com.kitlabs.androidbasicarchitecture.others.Toaster
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

object MembershipRequest {
    fun checkMembership(callBack: CallBack<UserMembershipKBean.Data>){

        RetrofitClient.getRequest().getMyMembership().enqueue(object : Callback<JsonObject?> {
            override fun onFailure(call: Call<JsonObject?>, t: Throwable) {
                callBack.onError("")
            }

            override fun onResponse(call: Call<JsonObject?>, response: Response<JsonObject?>?) {
                val code = response?.code() ?: 500;
                if (code == 200 && response?.isSuccessful == true) {
                    var userMembershipBean = Gson().fromJson(response?.body(), UserMembershipKBean::class.java)
                    callBack.onSuccess(userMembershipBean.data)
                } else if (code == 400 || code == 401) {
                    callBack.onSuccess(null)
                }
            }
        })
    }


    fun showMembershipExpiredDialog(context: Activity, data: UserMembershipKBean.Data?) {
        MembershipExpiredDialog(
            context,
            data?.validTo ?: "00",
            object : CallBack<Int>() {
                override fun onSuccess(t: Int?) {
                    if (t == 0) {
                        Toaster.shortToast("Please Update the plan!!")
                        context.finishAffinity()
                    } else {
                        //DataCache.get().clear()
                        context.startActivity(Intent(context, MembershipActivity::class.java))
                        context.finish()
                    }
                }
            }).show()
    }

    fun saveMembershipValidInToLocal(){
        SharedPref.get().save(KeyValue.IS_MEMEBERSHIP_PURCHASE,true)
    }
}