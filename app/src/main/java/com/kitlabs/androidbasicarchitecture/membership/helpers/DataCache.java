package com.kitlabs.androidbasicarchitecture.membership.helpers;

import java.util.HashMap;

public class DataCache {

    private static DataCache singleton;
    private boolean previouslyMembershipChecked;

    private DataCache() {
        previouslyMembershipChecked = false;
    }

    public synchronized static DataCache get() {
        if (singleton == null) {
            synchronized (DataCache.class) {
                if (singleton == null) {
                    singleton = new DataCache();
                }
            }
        }
        return singleton;
    }

    public void clear() {
        try {
            previouslyMembershipChecked = false;
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public boolean isPreviouslyMembershipChecked() {
        return previouslyMembershipChecked;
    }
}
