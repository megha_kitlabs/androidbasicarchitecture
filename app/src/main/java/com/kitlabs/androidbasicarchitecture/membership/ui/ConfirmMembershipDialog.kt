package com.kitlabs.androidbasicarchitecture.membership.ui

import android.app.Activity
import android.os.Bundle
import android.view.Window
import android.widget.RelativeLayout
import android.widget.TextView
import com.kitlabs.androidbasicarchitecture.R
import com.kitlabs.androidbasicarchitecture.base.BaseDialog
import com.kitlabs.androidbasicarchitecture.base.LogEnums
import com.kitlabs.androidbasicarchitecture.base.MyLogs
import com.kitlabs.androidbasicarchitecture.base.SharedPref
import com.kitlabs.androidbasicarchitecture.others.CallBack


class ConfirmMembershipDialog(context: Activity, private val membershipAmount: String, private val callBack: CallBack<Int>)
    : BaseDialog(context) {

    private val mCtx = context
    lateinit var sharedPref: SharedPref

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setCancelable(false)
        requestWindowFeature(Window.FEATURE_NO_TITLE)
        sharedPref = SharedPref(mCtx)
        setContentView(R.layout.dialog_confirm_membership)
        setDimBlur(window)
        val tvMemMessage: TextView = findViewById(R.id.tvMessage)
        val btMemCancel: TextView = findViewById(R.id.buttonCancel)
        val btnBuy: RelativeLayout = findViewById(R.id.buttonBuy)
        tvMemMessage.text = "Do you want to buy the membership plan of $$membershipAmount ?"
        btMemCancel.setOnClickListener {
            this@ConfirmMembershipDialog.cancel()
            MyLogs.addLog(LogEnums.LogType.MyInfo,
                "ConfirmMembershipDialog button #: Cancel",
                "Payment confirmation cancelled by user")
            callBack.onSuccess(0)
        }

        btnBuy.setOnClickListener {
            this@ConfirmMembershipDialog.cancel()
            MyLogs.addLog(LogEnums.LogType.MyInfo,
                "ConfirmMembershipDialog button #: Yes",
                "Payment confirmation initiated")
            callBack.onSuccess(1)
        }

    }
}