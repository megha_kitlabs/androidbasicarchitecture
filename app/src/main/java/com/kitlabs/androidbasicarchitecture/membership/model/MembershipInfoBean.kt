package com.kitlabs.androidbasicarchitecture.membership.model


import com.google.gson.annotations.SerializedName

data class MembershipInfoBean(
    @SerializedName("status")
    val status: String,
    @SerializedName("message")
    val message: String,
    @SerializedName("data")
    val `data`: Data
) {
    data class Data(
        @SerializedName("name")
        val name: String,
        @SerializedName("membership_frequency")
        val membershipFrequency: String,
        @SerializedName("google_actual_plan_id")
        val googleActualPlanId: String,
        @SerializedName("apple_actual_plan_id")
        val appleActualPlanId: String,
        @SerializedName("validFrom")
        val validFrom: String,
        @SerializedName("validTo")
        val validTo: String,
        @SerializedName("description")
        val description: String,
        @SerializedName("amountPaid")
        val amountPaid: Double,
        @SerializedName("status")
        val status: String,
        @SerializedName("subscription_status")
        val subscriptionStatus: String,
        @SerializedName("trial_taken")
        val trialTaken: Int,
        @SerializedName("trial_end_day")
        val trialEndDay: Any,
        @SerializedName("payment_platform")
        val paymentPlatform: String,
        @SerializedName("is_recurring")
        val isRecurring: Int,
        @SerializedName("isValid")
        val isValid: String
    )
}