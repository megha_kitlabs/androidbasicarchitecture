package com.kitlabs.androidbasicarchitecture.membership.ui

import android.app.Activity
import android.os.Bundle
import android.view.Window
import android.widget.RelativeLayout
import android.widget.TextView
import com.kitlabs.androidbasicarchitecture.R
import com.kitlabs.androidbasicarchitecture.base.BaseDialog
import com.kitlabs.androidbasicarchitecture.base.SharedPref
import com.kitlabs.androidbasicarchitecture.others.CallBack
import com.kitlabs.androidbasicarchitecture.others.ResourceUtils
import java.text.SimpleDateFormat
import java.util.*

class MembershipExpiredDialog(context: Activity, private val ending: String, private val callBack: CallBack<Int>)
    : BaseDialog(context) {
    var parseDate= SimpleDateFormat("MMM d,yy")
    var endDate= SimpleDateFormat("MMM d, yy")
    var getDateFormat = SimpleDateFormat("yyyy-MM-dd HH:mm:ss")
    var setDateFormat = SimpleDateFormat("MMM dd, yyyy")
    private val mCtx = context
    lateinit var sharedPref: SharedPref

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setCancelable(false)
        requestWindowFeature(Window.FEATURE_NO_TITLE)
        setContentView(R.layout.dialog_membership_expired)
        val tvCardMsg = findViewById<TextView>(R.id.tvCardMsg)
        val tvUpdatePlan = findViewById<RelativeLayout>(R.id.buttonUpdate)
        val date = try {
            setDateFormat.format(getDateFormat.parse(ending))
        } catch (e: Exception) {
            "--"
        }
        sharedPref = SharedPref(mCtx)
        setDimBlur(window)
        tvCardMsg.text ="${ResourceUtils.getString(R.string.msg_membership_expired)} $date"+" (EST)"

        tvUpdatePlan.setOnClickListener {
            this@MembershipExpiredDialog.cancel()
            callBack.onSuccess(1)
        }
    }

}