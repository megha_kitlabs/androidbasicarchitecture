package com.kitlabs.androidbasicarchitecture.membership.model

import com.google.gson.annotations.SerializedName

//data class MembershipBean(
//    val title: String,
//    val price: String,
//    val about: String,
//    val google_actual_plan_id: String,
//    val id: Int
//)
data class MembershipBean(
    @SerializedName("success")
    val success: String,
    @SerializedName("data")
    val `data`: List<Data>
) {
    data class Data(
        @SerializedName("id")
        val id: Int,
        @SerializedName("user_id")
        val userId: Int,
        @SerializedName("active_status")
        val activeStatus: Int,
        @SerializedName("name")
        val name: String,
        @SerializedName("description")
        val description: String,
        @SerializedName("price")
        val price: String,
        @SerializedName("membership_frequency")
        val membershipFrequency: String,
        @SerializedName("is_recurring")
        val isRecurring: Int,
        @SerializedName("braintree_actual_plan_id")
        val braintreeActualPlanId: String,
        @SerializedName("google_actual_plan_id")
        val googleActualPlanId: String,
        @SerializedName("apple_actual_plan_id")
        val appleActualPlanId: String,
        @SerializedName("includes_trial")
        val includesTrial: String,
        @SerializedName("trial_days")
        val trialDays: Any,
        @SerializedName("created_at")
        val createdAt: String,
        @SerializedName("updated_at")
        val updatedAt: String
    )
}