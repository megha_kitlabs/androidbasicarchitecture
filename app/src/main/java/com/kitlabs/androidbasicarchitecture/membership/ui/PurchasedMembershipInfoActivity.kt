package com.kitlabs.androidbasicarchitecture.membership.ui

import android.content.ActivityNotFoundException
import android.content.Intent
import android.content.IntentFilter
import android.net.ConnectivityManager
import android.net.Uri
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import com.kitlabs.androidbasicarchitecture.R
import com.kitlabs.androidbasicarchitecture.databinding.ActivityPurchasedMembershipInfoBinding
import com.kitlabs.androidbasicarchitecture.membership.viewmodel.MembershipInfoVM
import com.kitlabs.androidbasicarchitecture.others.NeTWorkChange
import com.kitlabs.androidbasicarchitecture.others.Toaster
import java.text.SimpleDateFormat

class PurchasedMembershipInfoActivity : AppCompatActivity() {

    lateinit var binding: ActivityPurchasedMembershipInfoBinding
    var viewModel: MembershipInfoVM = MembershipInfoVM()
    private val neTWorkChange = NeTWorkChange()
    var purchaseId: String? = null
    val getDateFormat = SimpleDateFormat("yyyy-MM-dd HH:mm:ss")
    val setDateFormat = SimpleDateFormat("MMM d, yyyy")

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView(this, R.layout.activity_purchased_membership_info)
        binding.viewModel = viewModel

        viewModel.getMembershipInfoData(this)

        setClicks()
        bindObserver()
    }

    private fun bindObserver() {
        viewModel.getMembershipLiveData?.observe(this, Observer {
            if (it != null) {
                binding.tvPlanType.text = it.data.name
                purchaseId = it.data.googleActualPlanId
                binding.webView.loadDataWithBaseURL(null, it.data.description,
                    "text/html", "utf-8", null)
                binding.tvStart.text = setDateFormat.format(getDateFormat.parse(it.data.validFrom))
                binding.tvEnd.text = setDateFormat.format(getDateFormat.parse(it.data.validTo))
            }
        })

        viewModel.errorLiveData.observe(this, Observer {
            Toaster.shortToast(it)
        })
    }

    private fun setClicks() {

        binding.buttonBack.setOnClickListener {
            onBackPressed()
        }

        binding.buttonCancel.setOnClickListener {
            openPlayStoreAccount()
        }
    }

    private fun openPlayStoreAccount() {
        val packageName = this.packageName
        try {
            val intent = Intent(
                Intent.ACTION_VIEW,
                Uri.parse("https://play.google.com/store/account/subscriptions?google_actual_plan_id=$purchaseId&package=$packageName")
            )
            startActivity(intent)
        } catch (e: ActivityNotFoundException) {
            Toaster.somethingWentWrong()
            e.printStackTrace()
        }
    }

    override fun onStart() {
        registerReceiver(neTWorkChange, IntentFilter(ConnectivityManager.CONNECTIVITY_ACTION))
        super.onStart()
    }
    override fun onStop() {
        unregisterReceiver(neTWorkChange);
        super.onStop()
    }
}