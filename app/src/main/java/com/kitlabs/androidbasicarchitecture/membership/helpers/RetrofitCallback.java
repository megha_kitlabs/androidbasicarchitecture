package com.kitlabs.androidbasicarchitecture.membership.helpers;

/**
 * Abstract Class for Callback which includes the onSuccess and onError methods which we are using the return once getting the response.
 * */
public abstract class RetrofitCallback<T,S> {
    public abstract void onMessage(T t,S t1,boolean success);

}
