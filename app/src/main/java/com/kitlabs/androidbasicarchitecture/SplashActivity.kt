package com.kitlabs.androidbasicarchitecture

import android.content.Intent
import android.os.Bundle
import android.os.Handler
import androidx.appcompat.app.AppCompatActivity
import com.kitlabs.androidbasicarchitecture.base.SharedPref
import com.kitlabs.androidbasicarchitecture.dashboard.dashboardData.ui.DashboardActivity
import com.kitlabs.androidbasicarchitecture.others.Cons
import com.kitlabs.androidbasicarchitecture.others.MyUtils
import com.kitlabs.androidbasicarchitecture.userAction.UserActivity

class SplashActivity : AppCompatActivity() {

    lateinit var sharedPref: SharedPref

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_splash)

        /** Check token
         * If token is not empty than move to Dashboard Screen
         * Else move to LogIn Screen
         * */

        sharedPref = SharedPref(applicationContext)
        val token = sharedPref.getStringValue(Cons.token)
        // we used the postDelayed(Runnable, time) method to send a user to login page after delayed time.
        Handler().postDelayed({
            when {
                MyUtils.isEmptyString(token) -> {
                    startActivity(Intent(this, UserActivity::class.java))
                    this.finish()
                }
                else -> {
                    startActivity(Intent(this, DashboardActivity::class.java))
                    this.finish()
                }
            }
        }, 3000) // 3000 is the delayed time in milliseconds.
    }

}