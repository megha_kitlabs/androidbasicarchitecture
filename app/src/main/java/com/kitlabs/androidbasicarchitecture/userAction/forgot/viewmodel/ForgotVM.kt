package com.kitlabs.androidbasicarchitecture.userAction.forgot.viewmodel

import android.util.Log
import android.util.Patterns
import android.widget.TextView
import androidx.databinding.BaseObservable
import androidx.databinding.ObservableField
import androidx.lifecycle.MutableLiveData
import com.airbnb.lottie.LottieAnimationView
import com.google.gson.Gson
import com.google.gson.JsonObject
import com.kitlabs.androidbasicarchitecture.base.BaseModel
import com.kitlabs.androidbasicarchitecture.network.RetrofitApi
import com.kitlabs.androidbasicarchitecture.network.RetrofitClient
import com.kitlabs.androidbasicarchitecture.others.Cons
import com.kitlabs.androidbasicarchitecture.others.MyUtils
import com.kitlabs.androidbasicarchitecture.userAction.forgot.model.ForgotBean
import org.json.JSONObject
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class ForgotVM : BaseObservable() {

    /** Initialize Text Field. */
    var email = ObservableField<String>()

    /** Initialize Validate Fields. */
    val isValidEmail = ObservableField(BaseModel(true))

    /** Initialize API Response. */
    var forgotPLiveData: MutableLiveData<ForgotBean>? = null
    var errorLiveData = MutableLiveData<String>()

    init {
        forgotPLiveData = MutableLiveData<ForgotBean>()
        errorLiveData = MutableLiveData<String>()
    }

    /**
     * On button click
     * @ResetPassword it will check first validations.
     * If all fields valid then Go for Api Access.
     * */
    fun submitData(tvSend: TextView, loader: LottieAnimationView) {
        if (checkValidation()) {
            val map: HashMap<String, Any> = HashMap()
            map[Cons.EMAIL] = email.get().toString().trim()
            resetPassword(map, tvSend, loader)
        }
    }

    /** Check Validation
     * If all fields valid then return true
     * */
    private fun checkValidation(): Boolean {
        var valid = false
        when {
            MyUtils.isEmptyString(email.get()) -> {
                isValidEmail.set(BaseModel(message = "Enter email"))
            }
            !Patterns.EMAIL_ADDRESS.matcher(email.get().toString().trim()).matches() -> {
                isValidEmail.set(BaseModel(message = "Please enter valid email address"))
            }
            else -> {
                valid = true
            }
        }
        return valid
    }

    /** Reset User Password*/
    private fun resetPassword(hashMap: HashMap<String, Any>, tvSend: TextView, loader: LottieAnimationView) {
        MyUtils.viewGone(tvSend)
        MyUtils.viewVisible(loader)
        val forgotRequest = RetrofitClient.getRegisterRetrofit().create(RetrofitApi::class.java)
        forgotRequest.forgotPassword(hashMap).enqueue(object : Callback<JsonObject> {

            override fun onResponse(call: Call<JsonObject>, response: Response<JsonObject>) {
                try{
                    if (response.code() == 200 && response.isSuccessful){
                        val loginResponse = Gson().fromJson(response.body(), ForgotBean::class.java)
                        forgotPLiveData?.value = loginResponse
                    }
                    else{
                        val jObjError = response.errorBody()?.string()?.let { JSONObject(it) }
                        errorLiveData.value = jObjError?.getString("message")
                    }
                } catch (e : Exception){
                    Log.e("OnException", e.localizedMessage)
                }
                MyUtils.viewGone(loader)
                MyUtils.viewVisible(tvSend)
            }

            override fun onFailure(call: Call<JsonObject>, t: Throwable) {
                Log.e("OnFailure", t.localizedMessage)
                MyUtils.viewGone(loader)
                MyUtils.viewVisible(tvSend)
            }

        })
    }

}