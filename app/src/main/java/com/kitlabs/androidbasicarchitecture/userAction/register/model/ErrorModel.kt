package com.kitlabs.androidbasicarchitecture.userAction.register.model


import com.google.gson.annotations.SerializedName

data class ErrorModel(
    @SerializedName("status")
    val status: String,
    @SerializedName("message")
    val message: Message
) {
    data class Message(
        @SerializedName("email")
        val email: String
    )
}