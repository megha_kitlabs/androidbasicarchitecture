package com.kitlabs.androidbasicarchitecture.userAction.login.ui

import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import com.kitlabs.androidbasicarchitecture.R
import com.kitlabs.androidbasicarchitecture.base.SharedPref
import com.kitlabs.androidbasicarchitecture.dashboard.dashboardData.ui.DashboardActivity
import com.kitlabs.androidbasicarchitecture.databinding.FragmentLoginBinding
import com.kitlabs.androidbasicarchitecture.others.Cons
import com.kitlabs.androidbasicarchitecture.others.CustomWatcher
import com.kitlabs.androidbasicarchitecture.others.Toaster
import com.kitlabs.androidbasicarchitecture.userAction.forgot.ui.ForgotPasswordActivity
import com.kitlabs.androidbasicarchitecture.userAction.login.model.LoginBean
import com.kitlabs.androidbasicarchitecture.userAction.login.socialLogin.SocialLoginHelper
import com.kitlabs.androidbasicarchitecture.userAction.login.viewmodel.LoginVM
import com.kitlabs.androidbasicarchitecture.userAction.register.ui.RegistrationFragment


class LoginFragment : SocialLoginHelper() {

    lateinit var binding : FragmentLoginBinding
    var viewModel: LoginVM = LoginVM()
    lateinit var sharedPref: SharedPref

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        arguments?.let {

        }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_login, container, false)
        val view = binding.root
        binding.userModel = viewModel
        sharedPref = SharedPref(context)

        onCreate()

        validateInputType()

        setClicks()

        bindObserver()

        return view
    }

    /**
     * observe API Status
     * If response success than Save data to shared preference
     * moving to next activity
     */
    private fun bindObserver() {
        viewModel.loginLiveData?.observe(requireActivity(), Observer {
            if (it != null) {
                if (it.status == "success") {
                    onLoginSuccess(it.data)
                    startActivity(Intent(requireContext(), DashboardActivity::class.java))
                    activity?.finish()
                } else {
                    Toaster.shortToast(it.message.toString())
                }
            }
        })

        viewModel.errorLiveData.observe(requireActivity(), Observer {
            Toaster.shortToast(it)
        })
    }

    /** save user data to shared preference at the time of login */
    private fun onLoginSuccess(it: LoginBean.Data) {
        sharedPref.save(Cons.firstName, it.firstName)
        sharedPref.save(Cons.lastName, it.lastName)
        sharedPref.save(Cons.name, it.name)
        sharedPref.save(Cons.user_email, it.email)
        sharedPref.save(Cons.token, it.token)
        sharedPref.save(Cons.phone, it.phone)
        sharedPref.save(Cons.location, it.location)
        sharedPref.save(Cons.imagePath, it.imagePath)
    }

    /** for handling clicks */
    private fun setClicks() {
        /** @LogIn click listener */
        binding.buttonLogin.setOnClickListener {
            viewModel.submitData(binding.tvLogin, binding.loader)
        }

        /** @ForgotPassword click listener */
        binding.tvForgotPassword.setOnClickListener {
            startActivity(Intent(requireContext(), ForgotPasswordActivity::class.java))
        }

        /** @SignUp click listener */
        binding.tvSignUpLogin.setOnClickListener {
            activity?.supportFragmentManager?.beginTransaction()
                ?.add(R.id.container_user_activity, RegistrationFragment())?.commit()
        }

        /** @Google click listener */
        binding.buttonGoogle.setOnClickListener {
            actionGoogleLogin()
        }

        /** @Facebook click listener */
        binding.buttonFacebook.setOnClickListener {
            actionLoginToFacebook()
        }

    }

    /** TextWatcher that watch a input text field and can instantly update data */
    private fun validateInputType() {
        binding.etEmailLogin.addTextChangedListener(CustomWatcher(binding.etEmailLogin,
                binding.tilEmailLogin, "Enter email", CustomWatcher.EditTextType.FirstName)
        )
        binding.etPasswordLogin.addTextChangedListener(
            CustomWatcher(binding.etPasswordLogin,
                binding.tilPasswordLogin, "Enter password", CustomWatcher.EditTextType.Password)
        )
    }

    /**
     * This method is override by SocialLoginHelper class
     * We are getting user data after login is successful
     */
    override fun onSocialLoginSuccess(socialId: String, name: String, email: String) {
        Toaster.shortToast("Success")
    }

}