package com.kitlabs.androidbasicarchitecture.userAction

import android.content.IntentFilter
import android.net.ConnectivityManager
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.kitlabs.androidbasicarchitecture.R
import com.kitlabs.androidbasicarchitecture.others.NeTWorkChange
import com.kitlabs.androidbasicarchitecture.userAction.login.ui.LoginFragment

class UserActivity : AppCompatActivity() {

    /** Initialize for @Internet Check */
    private val neTWorkChange = NeTWorkChange()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_user)

        /** @LoginFragment */
        supportFragmentManager.beginTransaction().add(R.id.container_user_activity, LoginFragment()).commit()
    }

    /** Check @Internet Status */
    override fun onStart() {
        registerReceiver(neTWorkChange, IntentFilter(ConnectivityManager.CONNECTIVITY_ACTION))
        super.onStart()
    }

    /** Check @Internet Status */
    override fun onStop() {
        unregisterReceiver(neTWorkChange);
        super.onStop()
    }
}