package com.kitlabs.androidbasicarchitecture.userAction.forgot.ui

import android.content.IntentFilter
import android.net.ConnectivityManager
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import com.kitlabs.androidbasicarchitecture.R
import com.kitlabs.androidbasicarchitecture.databinding.ActivityForgotPasswordBinding
import com.kitlabs.androidbasicarchitecture.others.CustomWatcher
import com.kitlabs.androidbasicarchitecture.others.NeTWorkChange
import com.kitlabs.androidbasicarchitecture.others.Toaster
import com.kitlabs.androidbasicarchitecture.userAction.forgot.viewmodel.ForgotVM

class ForgotPasswordActivity : AppCompatActivity() {

    lateinit var binding : ActivityForgotPasswordBinding
    var viewModel: ForgotVM = ForgotVM()
    private val neTWorkChange = NeTWorkChange()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView(this, R.layout.activity_forgot_password)
        binding.userModel = viewModel

        validateInputType()

        setClicks()

        bindObserver()

    }

    /**
     * observe API Status
     * According to the response show a toast
     */
    private fun bindObserver() {
        viewModel.forgotPLiveData?.observe(this, Observer {
            if (it != null) {
                Toaster.shortToast(it.message)
            }
        })

        viewModel.errorLiveData.observe(this, Observer {
            Toaster.shortToast(it)
        })
    }

    /** Check @Internet Status */
    override fun onStart() {
        registerReceiver(neTWorkChange, IntentFilter(ConnectivityManager.CONNECTIVITY_ACTION))
        super.onStart()
    }

    /** Check @Internet Status */
    override fun onStop() {
        unregisterReceiver(neTWorkChange);
        super.onStop()
    }

    /** for handling clicks */
    private fun setClicks() {

        /** @Send click listener */
        binding.buttonSend.setOnClickListener {
            viewModel.submitData(binding.tvSend, binding.loader)
        }

        /** Button @Back click listener */
        binding.buttonBack.setOnClickListener {
            onBackPressed()
        }

    }

    /** TextWatcher that watch a input text field and can instantly update data */
    private fun validateInputType() {
        binding.etEmailForgotP.addTextChangedListener(
            CustomWatcher(binding.etEmailForgotP,
            binding.tilEmailForgotP, "Enter email", CustomWatcher.EditTextType.FirstName)
        )
    }
}