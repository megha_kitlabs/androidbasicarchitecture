package com.kitlabs.androidbasicarchitecture.userAction.register.viewmodel

import android.util.Log
import android.util.Patterns
import android.widget.TextView
import androidx.databinding.BaseObservable
import androidx.databinding.ObservableField
import androidx.lifecycle.MutableLiveData
import com.airbnb.lottie.LottieAnimationView
import com.google.gson.Gson
import com.google.gson.JsonObject
import com.kitlabs.androidbasicarchitecture.base.BaseModel
import com.kitlabs.androidbasicarchitecture.network.RetrofitApi
import com.kitlabs.androidbasicarchitecture.network.RetrofitClient
import com.kitlabs.androidbasicarchitecture.others.CaseSensitiveHelper
import com.kitlabs.androidbasicarchitecture.others.Cons
import com.kitlabs.androidbasicarchitecture.others.MyUtils
import com.kitlabs.androidbasicarchitecture.userAction.register.model.SignUpBean
import org.json.JSONArray
import org.json.JSONObject
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response


class RegisterVM: BaseObservable() {

    /** Initialize Text Field. */
    var fName = ObservableField<String>()
    var lName = ObservableField<String>()
    var email = ObservableField<String>()
    var password = ObservableField<String>()
    var confirmPassword = ObservableField<String>()

    /** Initialize Validate Fields. */
    val isValidFName = ObservableField(BaseModel(true))
    val isValidLName = ObservableField(BaseModel(true))
    val isValidEmail = ObservableField(BaseModel(true))
    val isValidPassword = ObservableField(BaseModel(true))
    val isValidCPassword = ObservableField(BaseModel(true))

    /** Initialize API Response. */
    var registrationLiveData: MutableLiveData<SignUpBean>? = null
    var errorLiveData = MutableLiveData<String>()

    init {
        registrationLiveData = MutableLiveData<SignUpBean>()
        errorLiveData = MutableLiveData<String>()
    }

    /**
     * On button click
     * @SignUp it will check first validations.
     * If all fields valid then Go for Api Access.
     * */
    fun submitData(tvSignup: TextView, loader: LottieAnimationView) {
        if (checkValidation()) {
            val map: HashMap<String, Any> = HashMap()
            map[Cons.F_NAME] = CaseSensitiveHelper.returnCamelCaseWord(fName.get().toString().trim())
            map[Cons.L_NAME] = CaseSensitiveHelper.returnCamelCaseWord(lName.get().toString().trim())
            map[Cons.EMAIL] = email.get().toString().trim()
            map[Cons.PASSWORD] = password.get().toString().trim()
            map[Cons.REGISTER_TYPE] = "Native"
            map[Cons.DEVICE_TYPE] = "android"
            map[Cons.ROLE_TYPE] = "family"
            userSignUp(map, tvSignup, loader)
        }
    }

    /** Check Validation
     * If all fields valid then return true
     * */
    private fun checkValidation(): Boolean {
        var valid = false
        when {
            MyUtils.isEmptyString(fName.get()) -> {
                isValidFName.set(BaseModel(message = "Enter first name"))
            }
            MyUtils.isEmptyString(lName.get()) -> {
                isValidLName.set(BaseModel(message = "Enter last name"))
            }
            MyUtils.isEmptyString(email.get()) -> {
                isValidEmail.set(BaseModel(message = "Enter email"))
            }
            !Patterns.EMAIL_ADDRESS.matcher(email.get().toString().trim()).matches() -> {
                isValidEmail.set(BaseModel(message = "Please enter valid email address"))
            }
            MyUtils.isEmptyString(password.get()) -> {
                isValidPassword.set(BaseModel(message = "Enter password"))
            }
            password.get() != confirmPassword.get() -> {
                isValidCPassword.set(BaseModel(message = "Password mismatch"))
            }
            (password.get().toString().trim().length < 6) -> {
                isValidPassword.set(BaseModel(message = "Password should be at least minimum 6 characters"))
            }
            (password.get().toString().trim().length > 16) -> {
                isValidPassword.set(BaseModel(message = "Password should be maximum of 16 characters"))
            }
            else -> {
                valid = true
            }
        }
        return valid
    }

    /** Register User */
    private fun userSignUp(hashMap: HashMap<String, Any>, tvSignup: TextView, loader: LottieAnimationView) {
        MyUtils.viewGone(tvSignup)
        MyUtils.viewVisible(loader)
        val signUpRequest = RetrofitClient.getRegisterRetrofit().create(RetrofitApi::class.java)
        signUpRequest.registerWithEmail(hashMap).enqueue(object : Callback<JsonObject> {

            override fun onResponse(call: Call<JsonObject>, response: Response<JsonObject>) {
                try{
                    if (response.code() == 200 && response.isSuccessful){
                        val signUpResponse = Gson().fromJson(response.body(), SignUpBean::class.java)
                        registrationLiveData?.value = signUpResponse
                    }
                    else{
                        val jObjError = JSONObject(response.errorBody()?.string())
                        val message = JSONObject(jObjError?.getString("message"))
                        errorLiveData.value = JSONArray(message.getString("email"))[0].toString()
                    }
                } catch (e : Exception){
                    Log.e("OnException", e.localizedMessage)
                }
                MyUtils.viewGone(loader)
                MyUtils.viewVisible(tvSignup)
            }

            override fun onFailure(call: Call<JsonObject>, t: Throwable) {
                Log.e("OnFailure", t.localizedMessage)
                MyUtils.viewGone(loader)
                MyUtils.viewVisible(tvSignup)
            }

        })
    }
}