package com.kitlabs.androidbasicarchitecture.userAction.register.ui

import android.content.Intent
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import com.kitlabs.androidbasicarchitecture.R
import com.kitlabs.androidbasicarchitecture.base.SharedPref
import com.kitlabs.androidbasicarchitecture.dashboard.dashboardData.ui.DashboardActivity
import com.kitlabs.androidbasicarchitecture.databinding.FragmentRegistrationBinding
import com.kitlabs.androidbasicarchitecture.others.Cons
import com.kitlabs.androidbasicarchitecture.others.CustomWatcher
import com.kitlabs.androidbasicarchitecture.others.Toaster
import com.kitlabs.androidbasicarchitecture.userAction.login.ui.LoginFragment
import com.kitlabs.androidbasicarchitecture.userAction.register.model.SignUpBean
import com.kitlabs.androidbasicarchitecture.userAction.register.viewmodel.RegisterVM
import org.json.JSONArray
import org.json.JSONObject

class RegistrationFragment : Fragment() {

    lateinit var binding : FragmentRegistrationBinding
    var viewModel: RegisterVM = RegisterVM()
    lateinit var sharedPref: SharedPref

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        arguments?.let {

        }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_registration, container, false)
        val view = binding.root
        binding.userModel = viewModel
        sharedPref = SharedPref(context)
        validateInputType()

        setClicks()

        bindObserver()

        return view
    }

    /**
     * observe API Status
     * If response success than Save data to shared preference
     * moving to next activity
     */
    private fun bindObserver() {
        viewModel.registrationLiveData?.observe(requireActivity(), Observer {
            if (it != null) {
                if (it.status == "success") {
                    onSignUpSuccess(it.data)
                    startActivity(Intent(requireContext(), DashboardActivity::class.java))
                    activity?.finish()
                } else {
                    Toaster.shortToast(it.message)
                }
            }
        })

        viewModel.errorLiveData.observe(requireActivity(), Observer {
            Toaster.shortToast(it)
        })
    }

    /** save user data to shared preference at the time of SignUp */
    private fun onSignUpSuccess(it: SignUpBean.Data) {
        sharedPref.save(Cons.firstName, it.firstName)
        sharedPref.save(Cons.lastName, it.lastName)
        sharedPref.save(Cons.name, it.name)
        sharedPref.save(Cons.user_email, it.email)
        sharedPref.save(Cons.token, it.token)
        sharedPref.save(Cons.imagePath, it.imagePath)
    }

    /** for handling clicks */
    private fun setClicks() {

        /** @Signup click listener */
        binding.buttonSignup.setOnClickListener {
            viewModel.submitData(binding.tvSignup, binding.loader)
        }

        /** @LogIn click listener */
        binding.tvSignUpLogin.setOnClickListener {
            activity?.supportFragmentManager?.beginTransaction()
                ?.replace(R.id.container_user_activity, LoginFragment())?.commit()
        }

    }

    /** TextWatcher that watch a input text field and can instantly update data */
    private fun validateInputType() {
        binding.etFirstName.addTextChangedListener(
            CustomWatcher(binding.etFirstName,
                binding.tilFirstName, "Enter first name", CustomWatcher.EditTextType.FirstName)
        )
        binding.etLastName.addTextChangedListener(
            CustomWatcher(binding.etLastName,
                binding.tilLastName, "Enter last name", CustomWatcher.EditTextType.LastName)
        )
        binding.etEmailSignup.addTextChangedListener(
            CustomWatcher(binding.etEmailSignup,
            binding.tilEmailSignup, "Enter email", CustomWatcher.EditTextType.FirstName)
        )
        binding.etPasswordSignup.addTextChangedListener(
            CustomWatcher(binding.etPasswordSignup,
                binding.tilPasswordSignup, "Enter password", CustomWatcher.EditTextType.Password)
        )
        binding.etConfirmPasswordSignup.addTextChangedListener(
            CustomWatcher(binding.etConfirmPasswordSignup,
                binding.tilConfirmPasswordSignup, "Password mismatch", CustomWatcher.EditTextType.Password)
        )
    }

}