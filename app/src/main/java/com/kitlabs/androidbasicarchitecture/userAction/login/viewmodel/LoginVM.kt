package com.kitlabs.androidbasicarchitecture.userAction.login.viewmodel

import android.util.Log
import android.util.Patterns
import android.widget.TextView
import androidx.databinding.BaseObservable
import androidx.databinding.ObservableField
import androidx.lifecycle.MutableLiveData
import com.airbnb.lottie.LottieAnimationView
import com.google.gson.Gson
import com.google.gson.JsonObject
import com.kitlabs.androidbasicarchitecture.base.BaseModel
import com.kitlabs.androidbasicarchitecture.network.RetrofitApi
import com.kitlabs.androidbasicarchitecture.network.RetrofitClient
import com.kitlabs.androidbasicarchitecture.others.Cons
import com.kitlabs.androidbasicarchitecture.others.MyUtils
import com.kitlabs.androidbasicarchitecture.userAction.login.model.LoginBean
import org.json.JSONObject
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import javax.inject.Inject

class LoginVM : BaseObservable(){

    /** Initialize Text Field. */
    var email = ObservableField<String>()
    var password = ObservableField<String>()

    /** Initialize Validate Fields. */
    val isValidEmail = ObservableField(BaseModel(true))
    val isValidPassword = ObservableField(BaseModel(true))

    /** Initialize API Response. */
    var loginLiveData: MutableLiveData<LoginBean>? = null
    var errorLiveData = MutableLiveData<String>()

    init {
        loginLiveData = MutableLiveData<LoginBean>()
        errorLiveData = MutableLiveData<String>()
    }

    /**
     * On button click
     * @LogIn it will check first validations.
     * If all fields valid then Go for Api Access.
     * */
    fun submitData(tvLogin: TextView, loader: LottieAnimationView) {
        if (checkValidation()) {
            val map: HashMap<String, Any> = HashMap()
            map[Cons.EMAIL] = email.get().toString().trim()
            map[Cons.PASSWORD] = password.get().toString().trim()
            userLogin(map, tvLogin, loader)
        }
    }

    /** Check Validation
     * If all fields valid then return true
     * */
    private fun checkValidation(): Boolean {
        var valid = false
        when {
            MyUtils.isEmptyString(email.get()) -> {
                isValidEmail.set(BaseModel(message = "Enter email"))
            }
            !Patterns.EMAIL_ADDRESS.matcher(email.get().toString().trim()).matches() -> {
                isValidEmail.set(BaseModel(message = "Please enter valid email address"))
            }
            MyUtils.isEmptyString(password.get()) -> {
                isValidPassword.set(BaseModel(message = "Enter password"))
            }
            else -> {
                valid = true
            }
        }
        return valid
    }

    /** Login User */
    private fun userLogin(hashMap: HashMap<String, Any>, tvLogin: TextView, loader: LottieAnimationView) {
        MyUtils.viewGone(tvLogin)
        MyUtils.viewVisible(loader)
        val loginRequest = RetrofitClient.getRegisterRetrofit().create(RetrofitApi::class.java)
        loginRequest.loginWithEmail(hashMap).enqueue(object : Callback<JsonObject>{
            override fun onResponse(call: Call<JsonObject>, response: Response<JsonObject>) {
                try{
                    if (response.code() == 200 && response.isSuccessful){
                        val loginResponse = Gson().fromJson(response.body(), LoginBean::class.java)
                        loginLiveData?.value = loginResponse
                    }
                    else{
                        val jObjError = response.errorBody()?.string()?.let { JSONObject(it) }
                        errorLiveData.value = jObjError?.getString("message")
                    }
                } catch (e : Exception){
                    Log.e("OnException", e.localizedMessage)
                }
                MyUtils.viewGone(loader)
                MyUtils.viewVisible(tvLogin)
            }

            override fun onFailure(call: Call<JsonObject>, t: Throwable) {
                Log.e("OnFailure", t.localizedMessage)
                MyUtils.viewGone(loader)
                MyUtils.viewVisible(tvLogin)
            }
        })
    }

}