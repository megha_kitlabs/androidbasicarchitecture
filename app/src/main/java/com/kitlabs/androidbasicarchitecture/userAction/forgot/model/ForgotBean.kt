package com.kitlabs.androidbasicarchitecture.userAction.forgot.model


import com.google.gson.annotations.SerializedName

data class ForgotBean(
    @SerializedName("message")
    val message: String
)