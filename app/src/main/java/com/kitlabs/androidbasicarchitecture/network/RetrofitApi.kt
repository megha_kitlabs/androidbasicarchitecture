package com.kitlabs.androidbasicarchitecture.network

import com.google.gson.JsonObject
import com.kitlabs.androidbasicarchitecture.membership.model.MembershipBean
import com.kitlabs.androidbasicarchitecture.membership.model.MembershipInfoBean
import okhttp3.RequestBody
import retrofit2.Call
import retrofit2.http.*

interface RetrofitApi {

    /** @LogIn API */
    @FormUrlEncoded
    @POST("api/login")
    fun loginWithEmail(@FieldMap login: HashMap<String, Any>): Call<JsonObject>

    /** @SignUp API */
    @FormUrlEncoded
    @POST("api/register")
    fun registerWithEmail(@FieldMap signup: HashMap<String, Any>): Call<JsonObject>

    /** @ForgotPassword API */
    @FormUrlEncoded
    @POST("api/create")
    fun forgotPassword(@FieldMap forgot: HashMap<String, Any>): Call<JsonObject>

    /** @UserDetails API */
    @GET("api/get_user_data")
    fun getUserDetails(): Call<JsonObject>

    /** @UpdateProfile API */
    @POST("api/updateProfile")
    fun updateProfile(@Body requestBody: RequestBody): Call<JsonObject>

    /** @ChangePassword API */
    @FormUrlEncoded
    @POST("api/changePassword")
    fun changePassword(@FieldMap change: HashMap<String, Any>): Call<JsonObject>


    /**
     * Below Methods for Membership
     **/

    //Get Membership details like plan, price, details, etc.
    @GET("api/memberships")
    fun getMembershipDetails(): Call<JsonObject>

    @GET()
    fun getMembership(@Url url: String): Call<MembershipBean>

    @GET()
    fun getMembershipInfo(@Url url: String): Call<MembershipInfoBean>

    //Buy Membership
    @FormUrlEncoded
    @POST("api/buyMembership")
    fun purchaseMemberShip(@FieldMap hashMap: HashMap<String, String?>): Call<JsonObject>

    //For Membership Payment
    @FormUrlEncoded
    @POST("api/initialize_payment")
    fun initializePayment(@FieldMap map: HashMap<String, Any?>): Call<JsonObject>

    //Save Purchase Details
    @FormUrlEncoded
    @POST("api/save_inapp_payment")
    fun saveInAppPurchase(@FieldMap map: HashMap<String, Any?>): Call<JsonObject>

    //Membership Plan
    @GET("api/user_membership_plan")
    fun getMyMembership(): Call<JsonObject>
}
